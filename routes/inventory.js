const express = require('express');
const Auth = require('../middleware/auth');
const router = express.Router();
const userAuth = require('../middleware/userAuth')

const {
  addQuantity,
  removeQuantity,
  inventory1,
  productsByMachine,
  allInventory1
  // addInventory,
  // inventoryAll,
  // inventoryByCanteen,
  // inventoryByCompany,
  // ProductsInCompany,
  // deleteInventory,
  // inventoryByMachine,
  // inventoryByCompanyAndCanteen,
  // searchInventory,
  // inventoryByCompanyAndMachine,
  // inventoryByCanteenAndMachine,
  // updateInventory,
  // productsInMachine,
  // ProductsInCanteen,
  // ProductAll,
  // updateInventoryQuantity,
} = require("../controller/inventory");

router.post('/add/channel_id/:channel_id/product_id/:product_id', userAuth,Auth('product_add'), addQuantity);
router.patch('/remove/channel_id/:channel_id/product_id/:product_id', userAuth,Auth('product_remove') ,removeQuantity);
router.post("/all",userAuth,Auth('inventory_view'), allInventory1)



// router.get("compnay/:company_id?/canteen/:canteen_id?/machine/:machine_id?/product/:product_id?", inventory1)
// router.get("/company/:company_id?/canteen/:canteen_id?/machine/:machine_id?", inventory1)
// router.get("/company/:company_id?/canteen/:canteen_id?", inventory1)
// router.get("/company/:company_id?", inventory1)
// router.get("/canteen/:canteen_id?", inventory1)
// router.get("/machine/:machine_id?", inventory1)
// router.get("/machine/:machine_id?/product/:product_id?", inventory1)
// router.get("/company/:company_id?/product/:product_id?", inventory1)
// router.get("/canteen/:canteen_id?/product/:product_id?", inventory1)
// router.get("/product/:product_id?", inventory1)
// router.get("/", inventory1)
// router.get("/products/machine/:machine_id", productsByMachine)






module.exports = router; 