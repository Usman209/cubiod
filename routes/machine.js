
const express = require('express');
const Auth = require('../middleware/auth');
const router = express.Router();
const userAuth = require('../middleware/userAuth')
const {machineValidator}=require('../validator/machine')
// const { addMachine, listMachine, allMachine, displayMachineProduct, allMachineProductAdminSide, deleteMachine, updateMachine, addChannel, deleteMachineChannel, listMacineChannel,listMachineByCanteenId, displayMachineChannel, updateMachineChannel } = require('../controller/machine')


const { addMachine, listMachineByCanteenId, allMachineProductAdminSide, displayMachineProduct, listMachine, allMachine, deleteMachine, updateMachine, deleteMachineChannel, listMacineChannel, displayMachineChannel, updateMachineChannel } = require('../controller/machine')
// const { addChannel } = require('../controller/channel')


router.post('/', userAuth, Auth("machine_create"),machineValidator, addMachine);
router.get('/', userAuth, Auth("machine_view"), allMachine);

router.put('/:id', userAuth, Auth("machine_edit"), listMachine);
router.patch('/:id', userAuth, Auth("machine_edit"), updateMachine);
router.delete('/:id', userAuth, Auth("machine_delete"), deleteMachine);



router.get('/:id', userAuth, Auth("list-machines-by-canteen"), listMachineByCanteenId);

/////

/// Display Machine Product

router.get('/admin_side_product/', userAuth, Auth("admin-side-product"), allMachineProductAdminSide);
router.get('/product/:machine_id', userAuth, Auth("display-machine-product"), displayMachineProduct);


module.exports = router;
