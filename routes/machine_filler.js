
const express = require('express');
const { displayMachines,inventoryUpdateByMachineFiller, displayMachineFillerCanteens } = require('../controller/machine_filler');
const userAuth = require('../middleware/userAuth');
const { machineCanteen } = require('../controller/canteen');
const { displayMachineChannel } = require('../controller/channel')
const { userValidator } = require('../validator/user');
const Auth = require('../middleware/auth');
const router = express();


router.get('/:machine_id', userAuth, displayMachineChannel);



// Display Canteens under 1 Machine Filler
router.get('/canteens/:machine_filler_id', userAuth, displayMachineFillerCanteens);


// Display Machine under 1 canteen
router.get('/machine_filler_id/:machine_filler_id/canteen_id/:canteen_id', userAuth, displayMachines);

/// Update Inventory in machines
router.patch('/update-channels/:machine_id', userAuth, inventoryUpdateByMachineFiller);


module.exports = router;