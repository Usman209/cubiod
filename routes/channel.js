
const express = require('express');
const Auth = require('../middleware/auth');
const router = express.Router();
const userAuth = require('../middleware/userAuth')
const {channelValidator}=require('../validator/channel')
const { addChannel,addMachineRow,deleteMachineRow, unMergeMachineChannel, displayMachineChannel, mergeChannel, updateMachineChannel, deleteMachineChannel } = require('../controller/channel')


////////       Add Channel

// router.patch('/channel/:machine_id', userAuth, Auth(["add-channel",]), addChannel);

router.post('/machine_id/:machine_id', userAuth, Auth("channel_create"),channelValidator, addChannel);


/// Display Channel
router.get('/:machine_id', userAuth, Auth("channel_view"), displayMachineChannel);


router.patch('/:channel_id', userAuth, Auth("channel_edit"), updateMachineChannel);
router.delete('/:channel_id/machine_id/:machine_id/row_number/:row_number', userAuth, Auth("channel_delete"), deleteMachineChannel);
router.patch('/machine_id/:machine_id/merge_with/:merge_with',userAuth, Auth("channel_merge"), mergeChannel);


router.patch('/unmerge/channel_id/:channel_id', userAuth,Auth("channel_unmerge"), unMergeMachineChannel);


router.delete('/machine_id/:machine_id/row_number/:row_number', userAuth,Auth("row_delete"), deleteMachineRow);

router.post('/row/machine_id/:machine_id', userAuth,Auth("row_add"), addMachineRow);


// router.delete('/:channel_id/machine/:machine_id', userAuth, deleteMachineChannel);
// router.get('/:machine_id', userAuth, displayMachineChannel);
// router.put('/channel/:channel_id/machine/:machine_id', userAuth, listMacineChannel);


module.exports = router;
