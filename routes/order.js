

const express = require('express');
const { addOrder, alladminSideOrder, userOrders, deleteOrder } = require('../controller/order');
const userAuth = require('../middleware/userAuth')
const router = express.Router();


router.post('', userAuth, addOrder);

router.get('/', userAuth, alladminSideOrder);

router.get('/user/:id', userAuth, userOrders);

router.delete('/:id', userAuth, deleteOrder);

module.exports = router;