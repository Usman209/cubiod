const mongoose = require('mongoose');

const schema = mongoose.Schema(
  {

    machine_id: {
      type: mongoose.Types.ObjectId,
      ref: 'machine',
      required: [true, 'Machine Id must be required'],
    },
    row_number: {
      type: Number,
      required: [true, 'row number must be required']
    },
    channel_width: {
      type: Number,

      default: 1
    },
    merge_channel: [{
      type: mongoose.Types.ObjectId,
      default: null

    }],

  
    channel_name: {
      type: Number,
   required:true
    },

    channel_product_id: {
      type: mongoose.Types.ObjectId,
      ref:'products',
      default: null
    },
    channel_product_quantity: {
      type: Number,
      default: 0
    },
    channel_temperature: {
      type: String,
      default: null,
    },
    channel_extraction_time: {
      type: String,
      default: 0
    },
    channel_voltage: {
      type: String,
      default: null
    },
    channel_order: {
      type: Number,
      required:[true,"Channel Order must be required",422]
    },
    channel_status: {
      type: String,
      default: 'Active'

    },

  },
  { timestamps: true }
);

module.exports = mongoose.model('channels', schema);
