const mongoose = require('mongoose');

const schema = mongoose.Schema({
    machine_name: {
        type: String,
        required: true,
    },
    canteen_id: {
        type: mongoose.Types.ObjectId,
        ref: 'canteen',
        required: true,
    },
    machine_code: {
        type: String,
        required: true,
    },
    machine_status: {
        type: String,
        default: 'Active',
    },
    payment_method: {
        type:Array,
        default:['VIPPS','Paypal']
    },
    machine_location: {
        type: String,
     
    },

    machine_channel_column: {
        type: Number,
        default: 7
    },
    machine_channel_rows: {
        type: Number,
        default: 5
    },
}, { timestamps: true });

module.exports = mongoose.model('machine', schema);
