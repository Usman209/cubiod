const mongoose = require('mongoose');



const schema = mongoose.Schema({

    company_name: {
        type: String,
        required: [true, 'Company name must be required']
    },
    company_location: {
        type: String,
        required: [true, 'Company location must be required']
    },
    company_status: {
        type: String,
        default: 'Active'
    }



},
    { timestamps: true }
);


module.exports = mongoose.model('company', schema);