const mongoose = require('mongoose');

const schema = mongoose.Schema({
    user_id: {
        type: mongoose.Types.ObjectId,
        required: [true, 'user Id must be required']
    },
    name: {
        type: String,
        required: [true, 'Name required']
    },
    product: [
        {
            product_id: {
                type: mongoose.Types.ObjectId,
                ref: 'products',
                required: [true, 'Product Id must be required']
            },
            name: {
                type: String,
                required: [true, 'Product name must be required']
            },
            price: {
                type: Number,
                required: [true, 'Price required']
            },
            quantity: {
                type: Number,
                validate(value) {

                    if (value < 0) {
                        throw new Error('Order must have atleat one Product');
                    }
                }
            },
            discount: {
                type: Number,
                default: 0
            }

        }],
    totalPrice: {
        type: Number,
        default: 0,
        validate(value) {
            if (value < 0) {
                throw new mongoose.Error('order price must have more than 0')
            }
        }
    },
    status: {
        type: String,
        default: 'process'
    },
},
    { timestamps: true }
);

module.exports = mongoose.model('order', schema);