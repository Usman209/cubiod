const mongoose = require("mongoose");

const schema = mongoose.Schema({
  promo_name: {
    type: String,
    required: [true, "Please enter Promotion name"],
  },
  promo_code: {
    type: String,
   
    required: [true, "Please enter Promotion code "],
  },
  promo_description: {
    type: String,
    required: [true, "Please enter Promotion description"],
  },
  promo_start_date: {
    type: String,
    required: [true, "Please provide promotion starting date"],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  promo_end_date: {
    type: String,
    required: [true, "Please provide promotion expire Date"],
  },
  promo_type: {
    type: String,
    required: [true, "Please provide promotion type"],
  },
  promo_value: {
    type: Number,
  },
  promo_productid: {
    type: mongoose.Types.ObjectId,
    ref: "products",
  },
  promo_status: {
    type: String,
    required: [true, "Please provide promotion Status"],
  },
},{timestamps:true});

module.exports = mongoose.model("promotion", schema);
