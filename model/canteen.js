const mongoose = require("mongoose");

const schema = mongoose.Schema(
  {
    canteen_name: {
      type: String,
      required: true,
    },
    canteen_location: {
      type: String,
      required: true,
    },
    canteen_status: {
      type: String,
      default: 'Active'
    },
    food_supplier_id: {
      type: mongoose.Types.ObjectId,
      ref: 'user',
      default: null
    },
    machine_filler_id: {
      type: mongoose.Types.ObjectId,
      ref: 'user',
      default: null
    },
    canteen_admin_id:{
      type:mongoose.Types.ObjectId,
      ref:'user',
      default:null
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('canteen', schema);
