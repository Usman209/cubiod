
const validationResult = require('express-validator').validationResult;
const { check } = require('express-validator');
const res = require('express/lib/response');

const validate = (req, res, next) => {
  
    const errors = validationResult(req);


    if (!errors.isEmpty()) return res.status(422).json({success:false, errors: errors.array() });
    
    if (!req.file) {

        return res.status(422).json({success:false,errors:[{msg:'Category image must be required',param:'catagories_image'}]})
    }
    const image = req.file.path
    if (!image.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
const d=[{msg:"Only JPG ,PNG and JPEG file format is supported",param:'file'}]
        return res.status(422).json({success:false,errors:d})
    }

    next();
};


module.exports.catagoryValidator = [

    check('catagories_name').trim().notEmpty().withMessage('Catagories name must be required'),
    check('catagories_status').trim().notEmpty().withMessage('Catagories status must be required'),
    // check('catagories_image').trim().notEmpty().withMessage("Image must be required"),
    validate
];


