const validationResult = require('express-validator').validationResult;
const { check } = require('express-validator');

const validate = (req, res, next) => {
  
    const errors = validationResult(req);


    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });


    next();
};


module.exports.channelValidator = [
   check('row_number').trim().not().isEmpty().withMessage('channel row number must be required'),
  
    check('channel_product_id').trim().not().isEmpty().withMessage('product must be required'),
    check('channel_voltage').trim().notEmpty().withMessage('channel_voltage must be required'),
    check('channel_temperature').trim().notEmpty().withMessage('channel temperature  must be required'),
    check('channel_extraction_time').trim().notEmpty().withMessage('channel extraction time  must be required'),
    validate
];


