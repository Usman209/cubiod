
const validationResult = require('express-validator').validationResult;
const { check } = require('express-validator');

const validate = (req, res, next) => {
  
    const errors = validationResult(req);


    if (!errors.isEmpty()) return res.status(422).json({success:false, errors: errors.array() });


    next();
};


module.exports.permissionValidator = [

    check('permission_name').trim().notEmpty().withMessage('Permission name must be required'),
  
    validate
];


