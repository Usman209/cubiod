
const validationResult = require('express-validator').validationResult;
const { check } = require('express-validator');

const validate = (req, res, next) => {
  
    const errors = validationResult(req);


    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });


    next();
};


module.exports.roleValidator = [

    check('role_name').trim().notEmpty().withMessage('Role name must be required'),
    
    
    validate
];


