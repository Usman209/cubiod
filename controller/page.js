const mongoose = require("mongoose");

const pageModel = require("../model/page");
const ErrorHandler = require("../utils/errorHandling");
const asyncCatchHandler = require("../middleware/catchAsyncError");
exports.addPage = asyncCatchHandler(async (req, res, next) => {
  const { title, status, description, body,  category } = req.body;
  if (!title) {
    return next(new ErrorHandler("title must be required", 404));
  }
  if (!description) {
    return next(new ErrorHandler("description must be required", 404));
  }

  if (!status) {
    return next(new ErrorHandler("status must be required", 404));
  }
 
  let model = await new pageModel({
    title,
    status,
    description,
    body,
    category,
  });
  await model.save();

  res.status(200).json({
    success: true,
    message: "Page is added successfully",
    model,
  });
});

exports.allPage = asyncCatchHandler(async (req, res, next) => {
  const allPages = await pageModel.find();

  if (allPages.length < 1) {
    return next(new ErrorHandler("Pages not  found", 404));
  }

  res.json({
    success: true,
    allPages: allPages,
  });
});

exports.deletePage = asyncCatchHandler(async (req, res, next) => {
  const id = req.params.id;

  const page=await pageModel.findOne({_id:mongoose.Types.ObjectId(id),status:"Active"})

  if(!page){
    return next(new ErrorHandler("Page not found", 404));
  }
  page?page.status="InActive":""
  await page.save();
  return res.json({
    success:true,
    message:'Page is deleted successfully'
  })
  

});

exports.searchPage = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;

  const searchData = await pageModel.findById(id);

  if (!searchData) {
    return next(new ErrorHandler("Page not found", 404));
  }

  res.json({
    success: true,
    searchData,
  });
});
exports.updatePage = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;

  const { title, description,  body, status, category } = req.body;
  const page = await pageModel.findOne({_id:mongoose.Types.ObjectId(id)});
  if (!page) {
    return next(new ErrorHandler("Page not found", 404));
  }

  page ? (page.title = title) : title;
  description ? (page.description = description) : description;
  status ? (page.status = status) : status;
  body ? (page.body = body) : body;
  category ? (page.category = category) : category;
  page.save();
  res.json({
    success: true,
    message: "Page is updated successfully",
    page: page,
  });
});
