
const mongoose = require('mongoose');
const ErrorHandler = require('../utils/errorHandling')
const machine_model = require('../model/verdering');
const product_model = require('../model/product')
const asyncCatchHandler = require('../middleware/catchAsyncError')

const model = require('../model/channel');

exports.addChannel = asyncCatchHandler(async (req, res, next) => {
    const { machine_id } = req.params;

    const { row_number, channel_status, channel_product_id, channel_voltage, channel_product_quantity, channel_temperature, channel_extraction_time } = req.body
const {channel_position,position}=req.body


    const machineModel = await machine_model.findOne({ _id: mongoose.Types.ObjectId(machine_id),machine_status:'Active' })
    const productModel = await product_model.findOne({ _id: mongoose.Types.ObjectId(channel_product_id),product_status:'Active' })
    
    if (!productModel) {
        return next(new ErrorHandler('product not found', 404))
    }

    if (!machineModel) {
        return next(new ErrorHandler('Machine not found', 404))
    }

    const rowCount = await model.aggregate(
        [{$match: {
            machine_id: mongoose.Types.ObjectId(machine_id),
            row_number: Number(row_number),
            $or: [
             {
              channel_status: 'Active'
             },
             {
              channel_status: 'merged'
             }
            ]
           }}])
        
    if (rowCount.length >= 10) {
        return next(new ErrorHandler('Channel limit already filled', 404))
    }
  
const dataname=await model.findOne({machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number,channel_status:'Active'}).sort({channel_order:1}).limit(1)
if(!dataname){
    return next(new ErrorHandler('Channel row not found', 404))
}
// return(res.json(dataname))    
let channel_get_name=dataname.channel_name
let channel_order=dataname.channel_order
if(position=="start"){
   
        await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number},{$inc:{channel_order:1}}) 
        await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number,channel_name:{$gte:channel_get_name}},{$inc:{channel_name:1}}) 
        const channelModel = await new model({
            machine_id,
           
            row_number,
            channel_name:channel_get_name,
            channel_status,
            channel_product_id,
            channel_voltage,
            channel_product_quantity,
            channel_temperature,
            channel_extraction_time,
            channel_width: 1,
            channel_order:channel_order
        })
        await channelModel.save()
        return res.json({
            success: true,
            message: 'Channel is added successfully',
    
        })
    }
    if(position=="end"){
        const add_end=await model.find({machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number,$or:[{channel_status:'Active'},{channel_status:'merged'}]}).sort({channel_order:-1}).limit(1)
    //   const data=await model.find({$or:[{channel_status:'Active'},{channel_status:'merged'}],machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number})
    //   .sort({channel_order:-1}).limit(1)
    //     return res.json({end:data[0].channel_order})
    const dataGet=await model.findOne({machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number,channel_status:'Active'}).sort({channel_order:-1}).limit(1) 

    //  return(res.json(dataGet.channel_order+3))  
    const get_channel_name=dataGet.channel_name
    await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id),channel_name:{$gt:get_channel_name},row_number:row_number},{$inc:{channel_name:1}}) 
  
    const channelModel = await new model({
            machine_id,
          
            row_number,
            channel_name:Number(get_channel_name)+1,
            channel_status,
            channel_product_id,
            channel_voltage,
            channel_product_quantity,
            channel_temperature,
            channel_extraction_time,
            channel_width: 1,
            channel_order:Number(dataGet.channel_order+1)
        })
        await channelModel.save()
        return res.json({
            success: true,
            message: 'Channel is added successfully',
    
        })
    }
    if(!channel_position){
        return next(new ErrorHandler({msg:'Channel position must be required ',param:'channel_position' },404))
    }

    const column_position= await model.findOne({machine_id:mongoose.Types.ObjectId(machine_id),_id:mongoose.Types.ObjectId(channel_position),channel_status:'Active',row_number:Number(row_number)})
 
    if(!column_position){
        return next(new ErrorHandler('Channel position not found', 404))
    }
    const order_number= column_position.channel_order
    const channel_get_name1=column_position.channel_name;
 
//  return res.json(channel_get_name)  
 await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id),channel_name:{$gt:channel_get_name1},row_number:row_number},{$inc:{channel_name:1}}) 
  
   await model.updateMany({channel_order:{$gt:order_number},row_number:row_number,machine_id:mongoose.Types.ObjectId(machine_id)},{$inc:{channel_order:1}})
const channelModel = await new model({
        machine_id,
     
        row_number,
        channel_name:channel_get_name1+1,
        channel_status,
        channel_product_id,
        channel_voltage,
        channel_product_quantity,
        channel_temperature,
        channel_extraction_time,
        channel_width: 1,
        channel_order:order_number+1
    })
    await channelModel.save()


  
   return res.json({
        success: true,
        message: 'Channel is added successfully',

    })
})

exports.mergeChannel = asyncCatchHandler(async (req, res, next) => {
    const { machine_id, merge_with } = req.params;
    const { merge_to } = req.body
    if (merge_to.length < 1) {
        return next(new ErrorHandler('merge channel id must be required', 404))
    }
    let data = []

    for (let a in merge_to) {
        let abc = await model.findOne({
            machine_id: mongoose.Types.ObjectId(machine_id),
            _id: mongoose.Types.ObjectId(merge_to[a]),
            channel_status: 'Active'
        })

        if (!abc) {
            return next(new ErrorHandler(`This channel id ${merge_to[a]} not found in database`, 404))
        }
        if (abc.channel_product_quantity >= 1) {

            return next(new ErrorHandler(`You can't merge this channgle its already have product quantity `, 200))
        }

        data.push(abc._id)
    }

    const mergedNumber = Number(data.length)
    for (let a in merge_to) {
        await model.findOneAndUpdate({
            machine_id: mongoose.Types.ObjectId(machine_id),
            _id: mongoose.Types.ObjectId(merge_to[a])
        }, {
            channel_status: 'merged',
            //   merge_channel: merge_with 
        })


    }

    const data1 = await model.findOne({
        _id: mongoose.Types.ObjectId(merge_with)
    })

    if (!data1) {
        return next(new ErrorHandler(`This channel id ${merge_to[a]} not found in database`, 404))
    }

    data1 ? data1.channel_width = data1.channel_width + mergedNumber : channel_width;
    for (let a in data) {
        data1 ? data1.merge_channel = [...data1.merge_channel, data[a]] : data1.merge_channel
    }

    await data1.save()




    return res.json({
        success: true,
        message: 'Channel is merged successfully',

    })

})

/// Display Channel

exports.displayMachineChannel = asyncCatchHandler(async (req, res, next) => {

    const { machine_id } = req.params;


    const machineModel = await model.aggregate([{
        $match: {
            machine_id: mongoose.Types.ObjectId(machine_id),
            channel_status: 'Active'
        }
    }, {
        $lookup: {
            from: 'machines',
            localField: 'machine_id',
            foreignField: '_id',
            as: 'machine'
        }
    }, {
        $unwind: {
            path: '$machine',
            preserveNullAndEmptyArrays: true
        }
    }, {
        $lookup: {
            from: 'products',
            localField: 'channel_product_id',
            foreignField: '_id',
            as: 'products'
        }
    }, {
        $unwind: {
            path: '$products',
            preserveNullAndEmptyArrays: true
        }
    }, {
        $sort: {
            row_number: 1,
            channel_order: 1
        }
    }])
    if (machineModel && machineModel.length < 1) {
        return next(new ErrorHandler('Machine channel not found', 404))
    }
    res.json({
        success: true,
        channel: machineModel
    })
})


exports.deleteMachineChannel = asyncCatchHandler(async (req, res, next) => {

    const { channel_id, machine_id,row_number } = req.params;
  
    if (!row_number) {
        return next(new ErrorHandler("row number must be required", 404))
    }
    const channelCheck = await model.find({
        machine_id: machine_id, row_number: row_number,

        $or: [{ channel_status: 'Active' }, { channel_status: 'merged' }]
    })
if(channelCheck&& channelCheck.length<1){
    return next(new ErrorHandler("Channel not found", 404))
}
    if (channelCheck && channelCheck.length <= 1) {
        return next(new ErrorHandler("you can't delete this channel , row must have atleast one column", 404))
    }

    const channel = await model.findOne({ _id: mongoose.Types.ObjectId(channel_id),machine_id:mongoose.Types.ObjectId(machine_id), row_number: row_number, channel_status: 'Active' })

    if (!channel) {
        return next(new ErrorHandler('Channel not found', 404))
    }
  
   if(channel&&channel.merge_channel.length>=1){
    return next(new ErrorHandler('Please unmerge channels for delete', 200))
   }
    if (channel.channel_product_quantity >= 1) {
        return next(new ErrorHandler("you can't delete this channel,Channel have already products", 200))
    }
    channel ? channel.channel_status = "deleted" : channel.channel_status
    await channel.save()
    const channel_get_name2=channel.channel_name
    await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number,channel_name:{$gt:channel_get_name2}},{$inc:{channel_name:-1}}) 
    res.json({
        success: true,
        message: 'Channel is Deleted successfully'
    })
})
exports.updateMachineChannel = asyncCatchHandler(async (req, res, next) => {

    const { channel_id } = req.params;
    const {  channel_temperature, channel_product_id, channel_extraction_time, channel_voltage, channel_status } = req.body

    if (  !channel_temperature && !channel_product_id && !channel_extraction_time && !channel_voltage && !channel_status) {
        return next(new ErrorHandler('Channel data must be required for update', 404))
    }
    // const data = await model.findOne({ machine_id: machine_id })
    const verifyProduct=await product_model.findOne({_id:mongoose.Types.ObjectId(channel_product_id),product_status:'Active'})
if(!verifyProduct){
    return next(new ErrorHandler('Product not found', 404)) 
}   
    const channel = await model.findOne({ _id: mongoose.Types.ObjectId(channel_id), channel_status: 'Active' })
    if (!channel) {
        return next(new ErrorHandler('Channel not found', 404))
    }

  
    channel ? channel.channel_product_id = channel_product_id : channel.channel_product_id
    channel ? channel.channel_temperature = channel_temperature : channel.channel_channel_temperature
    channel ? channel.channel_extraction_time = channel_extraction_time : channel.channel_extraction_time
    channel ? channel.channel_status = channel_status : channel.channel_status
    channel ? channel.channel_voltage = channel_voltage : channel.channel_voltage

    await channel.save();
    return res.json({
        success: true,
        message: 'Channel is updated successfully',

    })




})

exports.unMergeMachineChannel = asyncCatchHandler(async (req, res, next) => {

    const { channel_id } = req.params;
    const channel = await model.findById({ _id: mongoose.Types.ObjectId(channel_id) })

    if (!channel) {
        return next(new ErrorHandler("channel not found", 200))
    }
    // channel.merge_channel
    if (channel.merge_channel.length < 1) {
        return next(new ErrorHandler("This channel don't have merge channel", 200))
    }
    const subChannel = channel.merge_channel
    for (let a in subChannel) {
        const data = await model.findByIdAndUpdate({ _id: mongoose.Types.ObjectId(subChannel[a]) }, {
            channel_status: 'Active'
        })

    }
    channel ? channel.merge_channel = [] : channel.merge_channel;
    channel ? channel.channel_width = 1 : channel.channel_width
    await channel.save()
    return res.json({
        success: true,
        message: 'channel is un_merged successfully'
    })

})



exports.deleteMachineRow = asyncCatchHandler(async (req, res, next) => {

    const {machine_id,row_number } = req.params;
  
    if (!row_number) {
        return next(new ErrorHandler("row number must be required", 404))
    }
    let isCheck=false
 const data1=await model.aggregate([{$match: {
 machine_id: mongoose.Types.ObjectId(machine_id),
 channel_status: 'Active'
}}, {$group: {
 _id: '$row_number',
 Totdal: {
  $push: {
   row_number: '$row_number'
  }
 }
}},{$project:{_id:1}}])
//    const data1=await model.distinct('row_number',{channel_status:'Active'})
    //   return res.json({data:data1})
  
   /// Checked If row number not matched
  data1&& data1.map(data=>{
    if(data._id==row_number){
        isCheck=true
    }
  })

if(!isCheck){
    return next(new ErrorHandler("row number not found", 404))
}
if(data1&& data1.length<=1){
    return next(new ErrorHandler("You can't delete this row,machine must have atleast 1 row", 404))
   }
//
    const channelCheck = await model.find({ row_number:row_number,machine_id:mongoose.Types.ObjectId(machine_id),
        channel_product_quantity:{$gt:0}
    })

 
if(channelCheck.length>=1){
   return res.json({
        success: false,
        message:`${channelCheck.length} Channel have already product in this Row. Please remove it to delete complete row.`,
        data:channelCheck
    })
}

// Delete Complete row with Channels
 await model.updateMany({ row_number:row_number,machine_id:mongoose.Types.ObjectId(machine_id)},{$set:{channel_status:'deleted'}},{multi: true})
/// Update after this rows channel names and row_number
 await model.updateMany({ row_number:{$gt:row_number},machine_id:mongoose.Types.ObjectId(machine_id)},{$inc:{row_number:-1,channel_name:-10}},{multi: true})
res.json({
        success: true,
        message: 'Channel Row is Deleted successfully',
        
    })
})



exports.addMachineRow = asyncCatchHandler(async (req, res, next) => {

    const {machine_id } = req.params;
 const {row_position,row_number}=req.body
 let row_number_checked=false
 const data1=await model.aggregate([{$match: {
    machine_id: mongoose.Types.ObjectId(machine_id),
    channel_status: 'Active'
   }}, {$group: {
    _id: '$row_number',
    Totdal: {
     $push: {
      row_number: '$row_number'
     }
    }
   }},{$project:{_id:1}}])

  
if(data1&& data1.length>=7){
    return next(new ErrorHandler("you can't create new row.you can create max 7 rows", 404))
}

 if(row_position=="start"){
    const data1=await model.aggregate([{$match: {
        machine_id: mongoose.Types.ObjectId(machine_id),
        channel_status: 'Active'
       }}, {$group: {
        _id: '$row_number',
        Totdal: {
         $push: {
          row_number: '$row_number'
         }
        }
       }},{$project:{_id:1}}])

     const update_rows=await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id)},{$inc:{row_number:1}});
     await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id)},{$inc:{channel_name:10}});
     for(let i=1; i<=7;i++){
    const new_row=await new model(
        {machine_id:mongoose.Types.ObjectId(machine_id),
            row_number:1,
            channel_order:i,
            channel_width:1,
           channel_name:10+i,
           channel_product_id:null,
           channel_product_quantity:"",
           channel_temperature:"",
           channel_extraction_time:null,
           channel_status:"Active",
           channel_voltage:null,
         } )
         await new_row.save();
        }
     return res.json({
        success: true,
        message: 'New row is added successfully',
     
    })

 }
 if(row_position=="end"){
     let big=1
    const data1=await model.aggregate([{$match: {
        machine_id: mongoose.Types.ObjectId(machine_id),
        channel_status: 'Active'
       }}, {$group: {
        _id: '$row_number',
        Totdal: {
         $push: {
          row_number: '$row_number'
         }
        }
       }},{$project:{_id:1}}])
let last_channel=await model.findOne({machine_id:mongoose.Types.ObjectId(machine_id),channel_status:'Active'}).sort({channel_name:-1}).limit(1)
    //   return res.json(last_channel)
       data1.forEach(a=>{
         if(big<a._id){
             big=a._id
         }
       })

    //  const update_rows=await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id)},{$inc:{row_number:1}});
// return res.json(last_channel.row_number)
let rowIndex=(last_channel.row_number+1)*10
// return res.json(rowIndex)
     for(let i=0; i<7;i++){
        rowIndex=rowIndex+1
    const new_row=new model(  {
        machine_id:mongoose.Types.ObjectId(machine_id),
            row_number:Number(big)+1,
            channel_order:i,
            channel_width:1,
           channel_name:rowIndex,
           channel_product_id:null,
           channel_product_quantity:0,
           channel_temperature:null,
           channel_extraction_time:"",
           channel_status:"Active",
           channel_voltage:null,
         } )
         await new_row.save();
         
        }
        return res.json({
            success: true,
            message: 'New row is added successfully',
            
        })

 }
 data1.forEach(a=>{
    if(row_number==Number(a._id)){
        row_number_checked=true
    }
   })
  if(!row_number_checked){
    return next(new ErrorHandler("row position not found", 404))
  } 
  let channel_final=await model.findOne({machine_id:mongoose.Types.ObjectId(machine_id),row_number:row_number,channel_status:'Active'}).sort({channel_name:-1})
//   return res.json(channel_final.row_number)
  let index=(channel_final.row_number+1)*10
  let updateIndex=(channel_final.row_number+1)*10
//   return res.json(index)
 await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id),row_number:{$gt:row_number}},{$inc:{channel_name:10}})
await model.updateMany({machine_id:mongoose.Types.ObjectId(machine_id),row_number:{$gt:Number(row_number)}},{$inc:{row_number:1}});
for(let i=1; i<=7;i++){
   
    const new_row=await new model(
        {machine_id:mongoose.Types.ObjectId(machine_id),
            row_number:Number(channel_final.row_number)+1,
            channel_order:i,
            channel_width:1,
           channel_name:index +i,
           channel_product_id:null,
           channel_product_quantity:0,
           channel_temperature:null,
           channel_extraction_time:"",
           channel_status:"Active",
           channel_voltage:null,
         } )
         await new_row.save();
        }
     return res.json({
        success: true,
        message: 'New row is added successfully',
        machine_id
    })
})