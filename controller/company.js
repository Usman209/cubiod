
// const ErrorHandler = require('../utils/errorHandling');
// const AsyncAwaitHandling = require('../middleware/catchAsyncError')
// const model = require('../model/company')
// const mongoose = require('mongoose')
// exports.addCompany = AsyncAwaitHandling(async (req, res, next) => {

//     const { company_name, company_location, company_status } = req.body;

//     if (!company_name) {
//         return next(new ErrorHandler('Company Name must be required', 422))
//     }
//     if (!company_location) {
//         return next(new ErrorHandler('Company location must be required', 422))
//     }

//     const companyModel = await new model({
//         company_name,
//         company_location,
//         company_status,
//     })

//     companyModel.save()
//     res.json({
//         success: true,
//         message: 'Company is added successfully'
//     })

// })
// exports.displayCompanies = AsyncAwaitHandling(async (req, res, next) => {


//     const companyData = await model.find({company_status:'Active'});

//     if (companyData.length < 1) {
//         return next(new ErrorHandler('Companies record not found',404 ) )
//     }

//    return res.json({
//         success: true,
//        companyData
//     })
// })

// exports.deleteCompany = AsyncAwaitHandling(async (req, res, next) => {

//     const { id } = req.params;

//     const company = await model.findOne({ _id: mongoose.Types.ObjectId(id), company_status: 'Active' });
//     if (!company) {
//         return next(new ErrorHandler('Company not found', 404))
//     }

//     company ? company.company_status = 'InActive' : company.company_status
//     await company.save()
//     return res.status(200).json({
//         success: true,
//         message: 'Company record is deleted successfully'
//     })

// })

// exports.SearchCompany = AsyncAwaitHandling(async (req, res, next) => {
//     const { id } = req.params;

//     const company = await model.findOne({_id:mongoose.Types.ObjectId(id),company_status:'Active'});

//     if (!company) {
//         return next(new ErrorHandler('Company not found', 404))
//     }

//     res.json({
//         success: true,
//         company
//     })
// })

// exports.updateCompany = AsyncAwaitHandling(async (req, res, next) => {
//     const { id } = req.params;
//     const { company_name, company_location, company_status } = req.body
   
//     if (!company_name && !company_location && !company_status) {
//         return next(new ErrorHandler('Company data must be required for update', 422))
//     }
//     const updateData = await model.findOne({_id:mongoose.Types.ObjectId(id),company_status:'Active'});
//     if (!updateData) {
//         return next(new ErrorHandler('Company not found', 404))
//     }

//     company_name ? updateData.company_name = company_name : updateData.company_name
//     company_location ? updateData.company_location = company_location : updateData.company_location
//     company_status ? updateData.company_status = company_status : updateData.company_status

//     updateData.save()
//     res.json({
//         success: true,
//         message: 'Company data is updated successfully',
//         updateData
//     })
// })

