const mongoose = require("mongoose");
const ErrorHandler = require("../utils/errorHandling");
const machine_model = require("../model/verdering");
const canteen_model = require("../model/canteen");
const product_model = require("../model/product");
const company_model = require("../model/company");
const channelModel=require('../model/channel')
const asyncCatchHandler = require("../middleware/catchAsyncError");
const model = require("../model/channel");
// const {allInventory1}=require('../routes/inventory')
// const machineModel = require("../model/verdering");


exports.allInventory1 = asyncCatchHandler(async (req, res,next) => {
const {machine_id,canteen_id,product_id}=req.body;
let invenoty=""

if(!machine_id && !canteen_id && !product_id){
invenoty=await product_model.aggregate([{$lookup: {
  from: 'channels',
  localField: '_id',
  foreignField: 'channel_product_id',
  as: 'string'
 }}, {$unwind: {
  path: '$string',
  preserveNullAndEmptyArrays: true
 }}, {$group: {
  _id: {
   _id: '$_id',
   product_name: '$product_name'
  },
  TotalQuantity: {
   $sum: '$string.channel_product_quantity'
  }
 }}, {$project: {
  product_name: '$_id.product_name',
  TotalQuantity: 1
 }}, {$project: {
  _id: 0
 }}])
 return res.json({
   success:true,
   data:invenoty
 })
}
if(machine_id && !canteen_id){
  return next(new ErrorHandler("Canteen must be required", 404));
}
if(canteen_id){
  const canteen=await canteen_model.findOne({_id:mongoose.Types.ObjectId(canteen_id)})
  if(!canteen){
    return next(new ErrorHandler("Canteen not found", 404));
  }
}
// Check inventoty for 1 machine 
if(machine_id && canteen_id && !product_id){

  const machine=await machine_model.findOne({canteen_id:mongoose.Types.ObjectId(canteen_id),_id:mongoose.Types.ObjectId(machine_id),machine_status:'Active'})

  if(!machine){
    return next(new ErrorHandler("No machine found with this canteen", 404));
  }
 
  invenoty=await machine_model.aggregate([{$match: {
    _id: mongoose.Types.ObjectId(machine_id)
   }}, {$lookup: {
    from: 'channels',
    localField: '_id',
    foreignField: 'machine_id',
    as: 'string'
   }}, {$unwind: {
    path: '$string',
    preserveNullAndEmptyArrays: true
   }}, {$match: {
    'string.channel_product_id': {
     $ne: null
    },
    'string.channel_product_quantity': {
     $gte: 1
    }
   }}, {$lookup: {
    from: 'products',
    localField: 'string.channel_product_id',
    foreignField: '_id',
    as: 'product'
   }}, {$unwind: {
    path: '$product',
    preserveNullAndEmptyArrays: true
   }}, {$project: {
    product_name: '$product.product_name',
    TotaQuantity: '$string.channel_product_quantity',
    product_id: '$product._id'
   }}, {$group: {
    _id: {
     _id: '$product_id',
     product_name: '$product_name'
    },
    TotalQuantity: {
     $sum: '$TotaQuantity'
    }
   }}, {$project: {
    product_name: '$_id.product_name',
    TotalQuantity: 1,
    _id:0
   }}]);
if(invenoty && invenoty.length<1){
  return res.json({
    success:true,
    message:'No product found'
  })
}
  return res.json({
  success:true,
  data:invenoty
})
} 
// Check inventory for particular product for machine
if(canteen_id&&machine_id && product_id){
  
  const machine=await machine_model.findOne({_id:mongoose.Types.ObjectId(machine_id),machine_status:'Active'})

  if(!machine){
    return next(new ErrorHandler("No machine found", 404));
  }

  invenoty=await channelModel.aggregate([{$match: {
    machine_id: mongoose.Types.ObjectId(machine_id),
    channel_product_id: mongoose.Types.ObjectId(product_id),
    channel_product_quantity: {
     $gte: 1
    }
   }}, {$group: {
    _id: '$channel_product_id',
    TotalQuantity: {
     $sum: '$channel_product_quantity'
    }
   }}, {$lookup: {
    from: 'products',
    localField: '_id',
    foreignField: '_id',
    as: 'products'
   }}, {$unwind: {
    path: '$products',
    preserveNullAndEmptyArrays: true
   }}, {$project: {
    _id: 1,
    TotalQuantity: 1,
    product_name: '$products.product_name'
   }}])

if(invenoty && invenoty.length<1){
  return res.json({
    success:false,
    message:'this product not found in machine',
  
  })
}
  return res.json({
  success:true,
  data:invenoty
})
}
// total product quantity in Cuboid
if(!canteen_id && !machine_id && product_id){
  invenoty=await channelModel.aggregate([{$match: {
    channel_product_id: mongoose.Types.ObjectId(product_id)
   }}, {$lookup: {
    from: 'products',
    localField: 'channel_product_id',
    foreignField: '_id',
    as: 'product'
   }}, {$unwind: {
    path: '$product',
    preserveNullAndEmptyArrays: true
   }}, {$group: {
    _id: {
     _id: '$product._id',
     product_name: '$product.product_name'
    },
    TotalQuantity: {
     $sum: '$channel_product_quantity'
    }
   }}, {$project: {
    product_name: '$_id.product_name',
    TotalQuantity: 1,
    _id: 0
   }}])
 if(invenoty&& invenoty.length<1){
   return res.json({
     success:true,
     message:"Product have 0 quantity"
   })
 }
 return res.json({
   success:true,
   data:invenoty
 })
}


// Inventory for complete canteen 
if(canteen_id && !machine_id && !product_id){

  const conteen=await canteen_model.findOne({_id:mongoose.Types.ObjectId(canteen_id)})
  if(!conteen){
    return next(new ErrorHandler("Canteen not found", 404));
  }
  invenoty=await machine_model.aggregate([{$match: {
    canteen_id: mongoose.Types.ObjectId(canteen_id)
   }}, {$lookup: {
    from: 'channels',
    localField: '_id',
    foreignField: 'machine_id',
    as: 'channels'
   }}, {$unwind: {
    path: '$channels',
    preserveNullAndEmptyArrays: true
   }}, {$match: {
    'channels.channel_product_id': {
     $ne: null
    },
    'channels.channel_product_quantity': {
     $gte: 1
    }
   }}, {$lookup: {
    from: 'products',
    localField: 'channels.channel_product_id',
    foreignField: '_id',
    as: 'product'
   }}, {$unwind: {
    path: '$product',
    preserveNullAndEmptyArrays: true
   }}, {$project: {
    machine_name: 1,
    _id: '$channels._id',
    product_name: '$product.product_name',
    TotalQuantity: '$channels.channel_product_quantity',
    product_id: '$product._id'
   }}, {$group: {
    _id: {
     _id: '$product_id',
     product_name: '$product_name'
    },
    TotalQuantity: {
     $sum: '$TotalQuantity'
    }
   }}, {$project: {
    product_name: '$_id.product_name',
    TotalQuantity: 1,
    _id:0
   }}])
if(invenoty && invenoty.length<1){
  return res.json({
    success:true,
    message:"Canteen don't have any product"
  })
}
   return res.json({
     success:true,
     data:invenoty
   })
} 

// Invertory for particular products for canteen
if(canteen_id && product_id && !machine_id){
  invenoty=await machine_model.aggregate([{$match: {
    canteen_id: mongoose.Types.ObjectId(canteen_id)
   }}, {$lookup: {
    from: 'channels',
    localField: '_id',
    foreignField: 'machine_id',
    as: 'channels'
   }}, {$unwind: {
    path: '$channels',
    preserveNullAndEmptyArrays: true
   }}, {$match: {
    'channels.channel_product_id': {
     $ne: null
    },
    'channels.channel_product_quantity': {
     $gte: 1
    }
   }}, {$lookup: {
    from: 'products',
    localField: 'channels.channel_product_id',
    foreignField: '_id',
    as: 'product'
   }}, {$unwind: {
    path: '$product',
    preserveNullAndEmptyArrays: true
   }}, {$project: {
    machine_name: 1,
    _id: '$channels._id',
    product_name: '$product.product_name',
    TotalQuantity: '$channels.channel_product_quantity',
    product_id: '$product._id'
   }}, {$match: {
    product_id: mongoose.Types.ObjectId(product_id)
   }}, {$group: {
    _id: {
     _id: '$product_id',
     product_name: '$product_name'
    },
    TotalQuantity: {
     $sum: '$TotalQuantity'
    }
   }}, {$project: {
    product_name: '$_id.product_name',
    TotalQuantity: 1,
    _id:0
   }}])
if(invenoty && invenoty.length<1){
  return res.json({
    success:true,
   message:'Product not found'
  })
}
   return res.json({
     success:true,
     data:invenoty
   })
}

return res.json(req.body)
});

























///////////

exports.inventory1 = asyncCatchHandler(async (req, res) => {
  // var { company_id,machine_id,canteen_id,product_id } = req.body;

  const machine_id = req.params.machine_id;
  const company_id = req.params.company_id;
  const canteen_id = req.params.canteen_id;
  const product_id = req.params.product_id;

  if (machine_id) {
    var response = await inventoryByMachine(machine_id);
  } else if (company_id) {
    response = await inventoryByCompany(company_id);
  } else if (canteen_id) {
    response = await inventoryByCanteen(canteen_id);
  } else if (!company_id && !canteen_id && !machine_id && !product_id) {
    response = await inventoryAll();
  } else if (company_id && canteen_id) {
    response = await inventoryByCompanyAndCanteen(company_id, canteen_id);
  } else if (company_id & machine_id) {
    response = await inventoryByCompanyAndMachine(company_id, canteen_id);
  } else if (canteen_id && machine_id) {
    response = await inventoryByCanteenAndMachine(canteen_id, machine_id);
  }
  // serach product from all companies
  else if (product_id) {
    // console.log('well 1234')
    response = await ProductAll(product_id);
  } else if (company_id && product_id) {
    response = await ProductsInCompany(company_id, product_id);
  } else if (canteen_id && product_id) {
    response = await ProductsInCanteen(canteen_id, product_id);
  } else if (machine_id && product_id) {
    response = await productsInMachine(machine_id, product_id);
  }

  res.json({
    success: true,
    Data: response,
    message: "response from here ",
  });
});










var inventoryByMachine = async (machine_id) => {
  let alreadyFoundIds = [];
  let productsQuantityList = {};
  // console.log("call from insdie ");

  value = mongoose.Types.ObjectId(machine_id);

  pk = await model.find({ machine_id: mongoose.Types.ObjectId(machine_id) });

  if (!pk) {
    return next(new ErrorHandler("No machine found", 404));
  }

  pk.forEach((d) => {
    if (!alreadyFoundIds.includes(d.channel_product_id)) {
      alreadyFoundIds.push(d.channel_product_id);

      productsQuantityList[d.channel_product_id] = { qty: 0, name: "" };
    }
  });

  for (let p = 0; p < pk.length; p++) {
    if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
      var searchData1 = await product_model.findById(pk[p].channel_product_id);

      if (pk[p].channel_product_id) {
        productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
          .channel_product_quantity;
        productsQuantityList[pk[p].channel_product_id].name =
          await searchData1.product_name;
      }
    }
  }

  delete productsQuantityList.null;

  // return await Promise.resolve(productsQuantityList);

  return productsQuantityList;
};

var inventoryByCanteen = async (canteen_id) => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};

  // value = mongoose.Types.ObjectId(machine_id);

  const data1 = await machine_model.find({ canteen_id: mongoose.Types.ObjectId(canteen_id) });

  if (data1.length < 1) {
    return "No canteen Found ";
  }

  for (let s = 0; s < data1.length; s++) {
    // console.log("machine data  :", data1[s]._id);

    pk = await model.find({
      machine_id:data1[s]._id,
    });

    //  // console.log("pk :", pk);

    if (!pk) {
      return next(new ErrorHandler("No machine found", 404));
    }

    pk.forEach((d) => {
      if (!alreadyFoundIds.includes(d.channel_product_id)) {
        alreadyFoundIds.push(d.channel_product_id);

        productsQuantityList[d.channel_product_id] = { qty: 0, name: "" };
      }
    });

    // // console.log(alreadyFoundIds);
    //// console.log("try4 :", productsQuantityList);

    for (let p = 0; p < pk.length; p++) {
      if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
        var searchData1 = await product_model.findById(
          pk[p].channel_product_id
        );

        if (pk[p].channel_product_id) {
          productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
            .channel_product_quantity;
          productsQuantityList[pk[p].channel_product_id].name =
            await searchData1.product_name;
        }
      }
    }
  }

  // console.log("products in one canteen : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);
  delete productsQuantityList.null;

  return productsQuantityList;
};

var inventoryByCompany = async (company_id) => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};
  // value = mongoose.Types.ObjectId(machine_id);

  const allCanteen = await canteen_model.find({
    canteen_company_id: company_id,
  });
  // console.log("length13 :", allCanteen.length);

  if (allCanteen.length < 1) {
    return "no canteen associate with this company";
  }

  for (let c = 0; c < allCanteen.length; c++) {
    const data1 = await machine_model.find({ canteen_id: allCanteen[c]._id });

    for (let k = 0; k < data1.length; k++) {
      // console.log("machine data  :", data1[k]._id);

      pk = await model.find({
        machine_id: mongoose.Types.ObjectId(data1[k]._id),
      });

      if (!pk) {
        return next(new ErrorHandler("No machine found", 404));
      }

      pk.forEach((d) => {
        if (d._id) {
          // // console.log('here10 :',d.channel_product_id)
          if (!alreadyFoundIds.includes(d.channel_product_id)) {
            alreadyFoundIds.push(d.channel_product_id);

            productsQuantityList[d.channel_product_id] = { qty: 0, name: "" };
          }
        }

        // console.log("product id missing  :");
      });

      for (let p = 0; p < pk.length; p++) {
        // // console.log("list 1 :", pk[p]);

        // for (let k = 0; k < pk[p].channel.length; k++) {
        if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
          // // console.log("now-list", pk[p].channel[k].channel_product_id);

          var searchData1 = await product_model.findById(
            pk[p].channel_product_id
          );

          if (pk[p].channel_product_id) {
            productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
              .channel_product_quantity;
            productsQuantityList[pk[p].channel_product_id].name =
              await searchData1.product_name;
          }
        }
        // }
      }
    }
  }

  delete productsQuantityList.null;

  return productsQuantityList;
};

var inventoryAll = async () => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};
  // value = mongoose.Types.ObjectId(machine_id);

  const companyData = await company_model.find();

  for (let t = 0; t < companyData.length; t++) {
    const allCanteen = await canteen_model.find({
      canteen_company_id: companyData[t]._id,
    });

    for (let c = 0; c < allCanteen.length; c++) {
      const data1 = await machine_model.find({ canteen_id: allCanteen[c]._id });

      for (let k = 0; k < data1.length; k++) {
        // console.log("machine data  :", data1[k]._id);

        pk = await model.find({
          machine_id: mongoose.Types.ObjectId(data1[k]._id),
        });

        if (!pk) {
          return next(new ErrorHandler("No machine found", 404));
        }

        pk.forEach((d) => {
          if (d._id) {
            if (!alreadyFoundIds.includes(d.channel_product_id)) {
              alreadyFoundIds.push(d.channel_product_id);

              productsQuantityList[d.channel_product_id] = { qty: 0, name: "" };
            }
          }

          // console.log("product id missing  :");
        });

        for (let p = 0; p < pk.length; p++) {
          if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
            var searchData1 = await product_model.findById(
              pk[p].channel_product_id
            );

            if (pk[p].channel_product_id) {
              productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
                .channel_product_quantity;
              productsQuantityList[pk[p].channel_product_id].name =
                await searchData1.product_name;
            }
          }
          // }
        }
      }
    }
  }

  // console.log("from  compnay list  : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);
  delete productsQuantityList.null;

  return productsQuantityList;
};

// compnay & canteen

var inventoryByCompanyAndCanteen = async (company_id, canteen_id) => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};
  company_id = mongoose.Types.ObjectId(company_id);
  canteen_id = mongoose.Types.ObjectId(canteen_id);
  // console.log(company_id, canteen_id);

  const company = await company_model.findById(company_id);

  if (!company) {
    return "company not found";
  }

  const Canteen = await canteen_model.find({
    canteen_company_id: company_id,
    _id: canteen_id,
  });

  // console.log("length13 :", Canteen.length);

  if (Canteen.length < 1) {
    return "no canteen associate with this company";
  }

  const data1 = await machine_model.find({
    canteen_id: canteen_id,
  });

  for (let k = 0; k < data1.length; k++) {
    // console.log("machine data  :", data1[k]._id);

    pk = await model.find({
      machine_id: mongoose.Types.ObjectId(data1[k]._id),
    });

    // // console.log('machine channels :', pk)

    // // console.log('one :',c,'id :', allCanteen[c]._id)
    // // console.log('machine data  :',data1[c]._id)
    // // console.log('namepro :',data1[c])

    // const data1 = await machine_model.find({ canteen_id: data1[c]._id });

    // if(data1.length < 1)
    // {
    //   return ('no machine associate with this canteen')
    // }

    // pk = await model.find({ machineId: mongoose.Types.ObjectId(machineId) });

    //  // console.log("pk :", pk);

    if (!pk) {
      return next(new ErrorHandler("No machine found", 404));
    }

    pk.forEach((d) => {
      if (d._id) {
        // // console.log('here10 :',d.channel_product_id)
        if (!alreadyFoundIds.includes(d.channel_product_id)) {
          alreadyFoundIds.push(d.channel_product_id);

          productsQuantityList[d.channel_product_id] = { qty: 0, name: "" };
        }
      }

      // console.log("product id missing  :");
    });

    //  // console.log('found :',alreadyFoundIds);
    // // console.log("try4 :", productsQuantityList);
    // // console.log("try5 :",  Object.keys(alreadyFoundIds).length);

    // if( Object.keys(alreadyFoundIds).length < 1 ){

    //   return ('there no produts in this channel')

    // }

    for (let p = 0; p < pk.length; p++) {
      // // console.log("list 1 :", pk[p]);

      // for (let k = 0; k < pk[p].channel.length; k++) {
      if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
        // // console.log("now-list", pk[p].channel[k].channel_product_id);

        var searchData1 = await product_model.findById(
          pk[p].channel_product_id
        );

        if (pk[p].channel_product_id) {
          productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
            .channel_product_quantity;
          productsQuantityList[pk[p].channel_product_id].name =
            await searchData1.product_name;
        }
      }
      // }
    }
  }

  // console.log("from  compnay list  : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);
  delete productsQuantityList.null;

  return productsQuantityList;
};

// company & machine

var inventoryByCompanyAndMachine = async (company_id, machine_id) => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};
  machine_id = mongoose.Types.ObjectId(machine_id);
  company_id = mongoose.Types.ObjectId(company_id);

  const allCanteen = await canteen_model.find({
    canteen_company_id: company_id,
  });
  // console.log("length13 :", allCanteen.length);

  if (allCanteen.length < 1) {
    return "no canteen associate with this company";
  }

  for (let c = 0; c < allCanteen.length; c++) {
    const data1 = await machine_model.find({ canteen_id: allCanteen[c]._id });

    for (let k = 0; k < data1.length; k++) {
      // console.log("here :", data1.length, k);

      if (data1[k]._id.equals(machine_id)) {
        pk = await model.find({
          machine_id: mongoose.Types.ObjectId(data1[k]._id),
        });

        if (!pk) {
          return next(new ErrorHandler("No machine found", 404));
        }

        pk.forEach((d) => {
          if (d._id) {
            // // console.log('here10 :',d.channel_product_id)
            if (!alreadyFoundIds.includes(d.channel_product_id)) {
              alreadyFoundIds.push(d.channel_product_id);

              productsQuantityList[d.channel_product_id] = {
                qty: 0,
                name: "",
              };
            }
          }

          // console.log("product id missing  :");
        });

        //  // console.log('found :',alreadyFoundIds);
        // // console.log("try4 :", productsQuantityList);
        // // console.log("try5 :",  Object.keys(alreadyFoundIds).length);

        // if( Object.keys(alreadyFoundIds).length < 1 ){

        //   return ('there no produts in this channel')

        // }

        for (let p = 0; p < pk.length; p++) {
          // // console.log("list 1 :", pk[p]);

          // for (let k = 0; k < pk[p].channel.length; k++) {
          if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
            // // console.log("now-list", pk[p].channel[k].channel_product_id);

            var searchData1 = await product_model.findById(
              pk[p].channel_product_id
            );

            if (pk[p].channel_product_id) {
              productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
                .channel_product_quantity;
              productsQuantityList[pk[p].channel_product_id].name =
                await searchData1.product_name;
            }
          }
          // }
        }
      }
    }
  }

  // console.log("from  compnay list  : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);
  delete productsQuantityList.null;

  return productsQuantityList;
};

// canteen & machine

var inventoryByCanteenAndMachine = asyncCatchHandler(
  async (canteen_id, machine_id) => {
    var alreadyFoundIds = [];
    var productsQuantityList = {};

    machine_id = mongoose.Types.ObjectId(machine_id);
    canteen_id = mongoose.Types.ObjectId(canteen_id);

    const data1 = await machine_model.find({ canteen_id: canteen_id });

    if (data1.length < 1) {
      return "No canteen Found ";
    }

    for (let s = 0; s < data1.length; s++) {
      // console.log("machine data  :", data1[s]._id);
      if (data1[s]._id.equals(machine_id)) {
        pk = await model.find({
          machine_id: mongoose.Types.ObjectId(data1[s]._id),
        });

        //  // console.log("pk :", pk);

        if (!pk) {
          return next(new ErrorHandler("No machine found", 404));
        }

        pk.forEach((d) => {
          if (!alreadyFoundIds.includes(d.channel_product_id)) {
            alreadyFoundIds.push(d.channel_product_id);

            productsQuantityList[d.channel_product_id] = { qty: 0, name: "" };
          }
        });

        // // console.log(alreadyFoundIds);
        //// console.log("try4 :", productsQuantityList);

        for (let p = 0; p < pk.length; p++) {
          if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
            var searchData1 = await product_model.findById(
              pk[p].channel_product_id
            );

            if (pk[p].channel_product_id) {
              productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
                .channel_product_quantity;
              productsQuantityList[pk[p].channel_product_id].name =
                await searchData1.product_name;
            }
          }
        }
      }
    }

    // console.log("products in one canteen : ", productsQuantityList);
    // console.log("products lenght : ", Object.keys(productsQuantityList).length);
    delete productsQuantityList.null;

    return productsQuantityList;
  }
);

// product over all

var ProductAll = async (product_id) => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};
  var productsQuantityList1 = {};

  // console.log("id :", product_id);
  product_ids = mongoose.Types.ObjectId(product_id);

  const companyData = await company_model.find();

  for (let t = 0; t < companyData.length; t++) {
    const allCanteen = await canteen_model.find({
      canteen_company_id: companyData[t]._id,
    });

    for (let c = 0; c < allCanteen.length; c++) {
      const data1 = await machine_model.find({ canteen_id: allCanteen[c]._id });

      for (let k = 0; k < data1.length; k++) {
        // console.log("machine data  :", data1[k]._id);

        pk = await model.find({
          machine_id: (data1[k]._id),
        });

        // console.log('pk :', pk )

        if (!pk) {
          return next(new ErrorHandler("No machine found", 404));
        }

        pk.forEach((d) => {
          if (d._id) {
            if (!alreadyFoundIds.includes(product_ids)) {
              alreadyFoundIds.push(product_ids);
              // console.log('d',d);
              // console.log('d.channel_product_id',d.channel_product_id);
              productsQuantityList[product_id] = {
                qty: 0,
                name: "",
              };
            }
          }
        });

        for (let p = 0; p < pk.length; p++) {
          {
            if (alreadyFoundIds.includes(product_ids)) {
              var searchData1 = await product_model.findById(
                pk[p].channel_product_id
              );

              if (product_ids.equals(pk[p].channel_product_id)) {
                
                // console.log('productsQuantityList',productsQuantityList,'productsQuantityList[pk[p].channel_product_id]',productsQuantityList[pk[p].channel_product_id], 'pk[p].channel_product_id',pk[p].channel_product_id);            
                productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
                  .channel_product_quantity;
                productsQuantityList[pk[p].channel_product_id].name =
                  await searchData1.product_name;
              }
            }
          }
        }
      }
    }
  }

  // console.log("from  compnay list  : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);
  delete productsQuantityList.null;

  return productsQuantityList;
};

// products in company

var ProductsInCompany = async (company_id, product_id) => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};
  product_ids = mongoose.Types.ObjectId(product_id);

  // value = mongoose.Types.ObjectId(machine_id);

  const allCanteen = await canteen_model.find({
    canteen_company_id: company_id,
  });
  // console.log("length13 :", allCanteen.length);

  if (allCanteen.length < 1) {
    return "no canteen associate with this company";
  }

  for (let c = 0; c < allCanteen.length; c++) {
    const data1 = await machine_model.find({ canteen_id: allCanteen[c]._id });

    for (let k = 0; k < data1.length; k++) {
      // console.log("machine data  :", data1[k]._id);

      pk = await model.find({
        machine_id: data1[k]._id,
      });

      // console.log('pk',pk);
      if (!pk) {
        return next(new ErrorHandler("No machine found", 404));
      }

      pk.forEach((d) => {
        if (d._id) {
          if (!alreadyFoundIds.includes(product_ids)) {
            alreadyFoundIds.push(product_ids);
            productsQuantityList[d.channel_product_id] = {
              qty: 0,
              name: "",
            };
          }
        }
      });

      //  // console.log('found :',alreadyFoundIds);
      // // console.log("try4 :", productsQuantityList);
      // // console.log("try5 :",  Object.keys(alreadyFoundIds).length);

      // if( Object.keys(alreadyFoundIds).length < 1 ){

      //   return ('there no produts in this channel')

      // }

      for (let p = 0; p < pk.length; p++) {
        {
          if (alreadyFoundIds.includes(product_ids)) {
            var searchData1 = await product_model.findById(
              pk[p].channel_product_id
            );
            // console.log(product_ids,pk[p].channel_product_id,product_id,productsQuantityList)

            if (product_ids.equals(pk[p].channel_product_id)) {
              productsQuantityList[product_id].qty += await pk[p]
                .channel_product_quantity;
              productsQuantityList[product_id].name =
                await searchData1.product_name;
            }
          }
        }
      }
    }
  }
  // console.log("from  compnay list  : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);
  delete productsQuantityList.null;

  return productsQuantityList;
};

// products in canteen

var ProductsInCanteen = async (canteen_id, product_id) => {
  var alreadyFoundIds = [];
  var productsQuantityList = {};
  product_ids = mongoose.Types.ObjectId(product_id);

  // value = mongoose.Types.ObjectId(machine_id);

  const data1 = await machine_model.find({ canteen_id: canteen_id });

  if (data1.length < 1) {
    return "No canteen Found ";
  }

  for (let s = 0; s < data1.length; s++) {
    // console.log("machine data  :", data1[s]._id);

    pk = await model.find({
      machine_id: mongoose.Types.ObjectId(data1[s]._id),
    });

      // console.log("pk :", pk);

    if (!pk) {
      return next(new ErrorHandler("No machine found", 404));
    }

    pk.forEach((d) => {
     
      // console.log('there ')
        if (!alreadyFoundIds.includes(product_ids)) {
          alreadyFoundIds.push(product_ids);
          productsQuantityList[d.channel_product_id] = {qty: 0,name: ""};
        }
      
    });

    // // console.log(alreadyFoundIds);
    // console.log("try4 :", productsQuantityList);

    for (let p = 0; p < pk.length; p++) {
      {
        // console.log('inside')
        if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
          var searchData1 = await product_model.findById(
            pk[p].channel_product_id
          );

          if (product_ids.equals(pk[p].channel_product_id)) {
            productsQuantityList[pk[p].channel_product_id].qty += await pk[p]
              .channel_product_quantity;
            productsQuantityList[pk[p].channel_product_id].name =
              await searchData1.product_name;
          }
        }
      }
    }
  }

  // console.log("products in one canteen : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);
  // delete productsQuantityList.null;

  return productsQuantityList;
};

// products in machine

var productsInMachine = async (machine_id, product_id) => {
  product_ids = mongoose.Types.ObjectId(product_id);

  pk = await model.find({ machine_id: mongoose.Types.ObjectId(machine_id) });

  // console.log("pk :", pk);

  if (!pk) {
    return next(new ErrorHandler("No machine found", 404));
  }
  var objects1 = {};
  var objects2 = {};

  // console.log(pk);
  let alreadyFoundIds = [];
  let productsQuantityList = {};

  pk.forEach((d) => {
    if (d._id) {
      if (!alreadyFoundIds.includes(product_ids)) {
        alreadyFoundIds.push(product_ids);
        productsQuantityList[d.channel_product_id] = {
          qty: 0,
          name: "",
        };
      }
    }
  });
  // console.log(alreadyFoundIds);
  // console.log("try4 :", productsQuantityList);

  for (let p = 0; p < pk.length; p++) {
    {
      if (alreadyFoundIds.includes(product_ids)) {
        var searchData1 = await product_model.findById(
          pk[p].channel_product_id
        );

        if (product_ids.equals(pk[p].channel_product_id)) {
          productsQuantityList[product_id].qty += await pk[p]
            .channel_product_quantity;
          productsQuantityList[product_id].name =
            await searchData1.product_name;
        }
      }
    }
  }

  // console.log("products in one machine : ", productsQuantityList);
  // console.log("products lenght : ", Object.keys(productsQuantityList).length);

  // sk=_.omit(productsQuantityList.Data, "null"); //It will return a new object
  return productsQuantityList;
};

exports.addQuantity = asyncCatchHandler(async (req, res, next) => {
  const { channel_id, product_id } = req.params;
  const { channel_product_quantity } = req.body;
  if (!channel_product_quantity) {
    return next(new ErrorHandler("product quantity must be required", 404));
  }
  // const data = await model.findOne({ machine_id: machine_id })
  const channel = await model.findOne({
    _id: mongoose.Types.ObjectId(channel_id),
    channel_status: "Active",
  });
  if (!channel) {
    return next(new ErrorHandler("Channel not found", 200));
  }
  // console.log(product_id, channel.channel_product_id);
  if (channel.channel_product_id != product_id) {
    return next(new ErrorHandler("This channel has not this product", 404));
  }
  channel ? (channel.channel_product_quantity = channel.channel_product_quantity + channel_product_quantity): channel.channel_product_quantity;

  await channel.save();
  return res.json({
    success: true,
    message: "Product quantity  updated successfully",
  });
});

/// Remove Product Quantity in Machine Channel

exports.removeQuantity = asyncCatchHandler(async (req, res, next) => {
  const { channel_id, product_id } = req.params;
  const { channel_product_quantity } = req.body;
  if (!channel_product_quantity) {
    return next(new ErrorHandler("product quantity must be required", 404));
  }
  // const data = await model.findOne({ machine_id: machine_id })
  const channel = await model.findOne({
    _id: mongoose.Types.ObjectId(channel_id),
    channel_status: "Active",
  });
  if (!channel) {
    return next(new ErrorHandler("Channel not found", 200));
  }

  if (channel.channel_product_id != product_id) {
    return next(new ErrorHandler("This channel has not this product", 404));
  }
  if (channel.channel_product_quantity < channel_product_quantity) {
    return next(new ErrorHandler("Product quantity is less in channel", 404));
  }
  channel
    ? (channel.channel_product_quantity =
        channel.channel_product_quantity - channel_product_quantity)
    : channel.channel_product_quantity;

  await channel.save();
  return res.json({
    success: true,
    message: "Product quantity removed successfully",
  });
});

// exports.inventoryByMachine = asyncCatchHandler(async (req, res, next) => {
//   const { machine_id } = req.params;

//   value = mongoose.Types.ObjectId(machine_id);

//   // console.log("here is :", value);

//   pk = await model.find({ machine_id: mongoose.Types.ObjectId(machine_id) });

//   // console.log("pk :", pk);

//   if (!pk) {
//     return next(new ErrorHandler("No machine found", 404));
//   }

//   arr = [];
//   var objects = {};

//   for (let i = 0; i < pk.length; i++) {

//     // // console.log('tk : ',pk[i].channel.length)

//     for (let j = 0; j < pk[i].channel.length; j++) {

//       // console.log(pk[i].channel[j].channel_name)
//       arr.push(pk[i].channel[j].channel_product_id)
//       if (pk[i].channel[j].channel_product_id) {
//         var searchData = await product_model.findById(pk[i].channel[j].channel_product_id);

//         // console.log(' name: ', searchData.name)

//         objects[j] = {
//           productId: pk[i].channel[j].channel_product_id,
//           qty: pk[i].channel[j].channel_product_quantity,
//           p_name: searchData.name ? searchData.name : "no"
//         };
//       }

//     }
//   }

//   // console.log("try3 ", objects);

//   ({
//     success: true,
//     pk: pk,
//   });
// });

// Display All product with quantity in admin Side

// exports.addInventory = asyncCatchHandler(async (req, res, next) => {

//     const { productId, quantity, machineId, channel_id } = req.body;

//     if (!productId) {
//         return next(new ErrorHandler('product Id must be required', 404))
//     }

//     if (!quantity) {
//         return next(new ErrorHandler('product quantity must be required', 404))
//     }
//     const inventoryModel = new model({
//         productId,
//         quantity,
//         machineId,
//         channel_id
//     })

//     await inventoryModel.save();
//     ({
//         success: true,
//         message: 'Product Inventory is added successfully'
//     })

// })
/// In Admin Side All inventory

// exports.deleteInventory = asyncCatchHandler(async (req, res, next) => {

//     const { id } = req.params;

//     const deleteModel = await model.findByIdAndDelete(id);

//     if (!deleteModel) {
//         return next(new ErrorHandler('No inventory found', 404))

//     }
//     ({
//         success: true,
//         message: 'Inventory is deleted successfully'
//     })
// }

// )
// exports.updateInventory = asyncCatchHandler(async (req, res, next) => {

//     const { id } = req.params;
//     let { reservation } = req.body

//     if (reservation) {
//         Number(reservation)
//     }

//     const updateModel = await model.findById(id);

//     if (!updateModel) {
//         return next(new ErrorHandler('Inventory not found', 404))

//     }
//     if (reservation && updateModel.quantity < reservation) {
//         return next(new ErrorHandler('Product is out of stock', 404))

//     }
//     reservation ? updateModel.quantity = updateModel.quantity - Number(reservation) : updateModel.quantity;
//     reservation ? updateModel.reservation = updateModel.reservation + Number(reservation) : updateModel.reservation

//     updateModel.save();

//     ({
//         success: true,
//         message: 'Inventory is updated successfully'
//     })

// })

// exports.updateInventoryQuantity = asyncCatchHandler(async (req, res, next) => {

//     const { id } = req.params;
//     let { quantity } = req.body

//     if (quantity) {
//         Number(quantity)
//     }

//     const updateModel = await model.findById(id);

//     if (!updateModel) {
//         return next(new ErrorHandler('No inventory found', 404))

//     }

//     quantity ? updateModel.quantity = Number(quantity) : updateModel.quantity;
//     updateModel.save();

//     ({
//         success: true,
//         message: 'Inventory is updated successfully'
//     })

// })

//////////////////

exports.allInventory = asyncCatchHandler(async (req, res, next) => {
  const inventory = await model.aggregate([
    {
      $group: {
        _id: "$productId",
        Assigned: {
          $sum: "$quantity",
        },
      },
    },
    {
      $lookup: {
        from: "products",
        localField: "_id",
        foreignField: "_id",
        as: "product",
      },
    },
    {
      $unwind: {
        path: "$product",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $project: {
        _id: "$_id",
        Assigned: 1,
        product: 1,
        TotalProduct: {
          $add: ["$Assigned", "$product.quantity"],
        },
      },
    },
  ]);

  if (inventory.length < 1) {
    return next(new ErrorHandler("No inventory found", 404));
  }

  res.json({
    success: true,
    inventory,
  });
});

exports.searchInventory = asyncCatchHandler(async (req, res, next) => {
  const { machine_id, product_id } = req.params;

  const searchModel = await model.aggregate([
    {
      $match: {
        productId: mongoose.Types.ObjectId(product_id),
        machineId: mongoose.Types.ObjectId(machine_id),
      },
    },
    {
      $group: {
        _id: "$productId",
        TotalProduct: {
          $sum: "$quantity",
        },
      },
    },
  ]);

  if (!searchModel) {
    return next(new ErrorHandler("No inventory found", 404));
  }
  res.json({
    success: true,
    searchModel,
  });
});


exports.productsByMachine = asyncCatchHandler(async (req, res, next) => {
  const machine_id = req.params.machine_id;
  let alreadyFoundIds = [];
  let productsQuantityList = []; 
  value = mongoose.Types.ObjectId(machine_id); 
  pk = await model.find({ machine_id: mongoose.Types.ObjectId(machine_id) }); 
  if (!pk) {
    return next(new ErrorHandler("No machine found", 404));
  }
  pk.forEach((d) => {
    if (!alreadyFoundIds.includes(d.channel_product_id)) {
      alreadyFoundIds.push(d.channel_product_id);
    }
  });
  for (let p = 0; p < pk.length; p++) {
    if (alreadyFoundIds.includes(pk[p].channel_product_id)) {
      var searchData1 = await product_model.findById(pk[p].channel_product_id);

      if (pk[p].channel_product_id) {
        productsQuantityList.push({
          product_name: searchData1.product_name,
          _id: searchData1._id,
          price: searchData1.product_price,
          vat: searchData1.product_VAT,
          allergic: searchData1.prdouct_allergic,
          status: searchData1.product_status,
          recipe: searchData1.product_recipe,
        });  
      }
    }
  } 
  productsQuantityList = productsQuantityList.filter((v, i, a) => a.findIndex((t) => t._id.toString() === v._id.toString()) === i);  
  res.json({
    Data: productsQuantityList,
  });
});
