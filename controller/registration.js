const {  validationResult } = require('express-validator');
const bcrypt = require('bcryptjs')

const user = require('../model/userModel')
const mongoose=require('mongoose');
const roleModel=require('../model/roles')
// const { emailRegistration } = require('../email/account')
const jwt = require('jsonwebtoken')
// const { forgotPassword } = require('../email/account')
const ErrorHandler = require('../utils/errorHandling')
const asyncCatchHandler = require('../middleware/catchAsyncError');

exports.signupController = asyncCatchHandler(async (req, res, next) => {

    const { user_name, user_email, user_phone,user_role_id,user_role, user_password } = req.body;
 
    const errors = validationResult(req);

    if (errors.errors.length > 0) {

        if (errors.errors.length > 1) {
            errors.errors.map(msg=>{
                res.json({err:msg.msg})
            })
            res.json({ error: errors.errors })
        } else {
            errors.errors.map(data => {

                return res.json({
                    error_aya: data.msg

                })


            })
        }
    }  else {
      
        const dublicate = await user.find({ user_email: user_email })

        if (dublicate.length > 0) {
            return res.json({
                success:false,
                message: "Email is already register"
            })
        } else {
            const hash = await bcrypt.hash(user_password, 10);
           
                const model = new user({
                    user_name,
                    user_email,
                    user_phone,
                    user_password: hash,
                    user_role_id,
                    user_role
                })


await model.save()
return res.json({
    success:true,
    message:'User is registered successfully'
})

// //// Email Registration


//  emailRegistration(data.name, data.email)


        }
    }
  
})


exports.loginController = async(req, res) => {

    const { user_email, user_password } = req.body;


   const data= await user.find({ user_email: user_email })
       
        if (data.length < 1) {
            return res.json({
                success:false,
                message: 'user name or password is wrong'
            })
        }
     
    
      const result = await bcrypt.compare(user_password, data[0].user_password)
            
            if (result) {
                
                const token = jwt.sign({
                    data: data[0]._id,
                }, 'secret', { expiresIn: 60000 * 600000 });

                // const exampleFilter = ({ keepMe, keepMeToo }) => ({ keepMe, keepMeToo })
              

                const userData = await user
                .findOne({_id:mongoose.Types.ObjectId(data[0]._id)})
                .select({user_password:0});


      const permissions=userData&&userData.user_role_id?await roleModel.findOne({_id:mongoose.Types.ObjectId(userData.user_role_id)}):[]

            

                return res.json({
                    success:true,
                    message: 'user is signin successfully',
                    token: token,
                    user:userData,
                    permissions:permissions

                })
            } else {
                return res.json({
                    success:false,
                    message: 'user name or password is wrong'
                })
            }
       

    // next()
}

exports.resetController = async (req, res) => {
    try {
        
        const token = req.body.token
        const user_password = req.body.user_password;
        if(!token){
            return res.json({
                success:false,
                error:'token must be required'
            })
        }
        if (!user_password) {
            return res.json({success:false, error: 'password must be required' })
        }
        const verify = await jwt.verify(token, 'secret')


        if (!verify) {
            return res.json({
                success:'false',
                error: "Token is not valid"
            })
        }

      const userObj = await user.findOne({ _id: verify.id,user_status:'Active' })
      if (!userObj)    return res.json({success:false,error:"User not found!"})
      if(userObj&& userObj.user_token!=token){
        return res.json({success:false,error:"Unauthorized "})
      }
        if(user_password.length<6) return res.json({success:false,error:"Password must be at least 6 chars long"})
const hash=await bcrypt.hash(user_password,10)
        userObj.user_password = hash;
        userObj.user_token=""
        userObj.save();

        return res.json({
            success:true,
            message:'User password is reset successuflly'
        })
    } catch (err) {

        res.json({success:false, error: err.message })
    }

}


exports.forgotController =async (req, res) => {

    const user_email = req.body.user_email;

    if(!user_email){
        return res.json({
            success:false,
            message:'user email must be required'
        })
    }

  const data= await user.findOne({ user_email: user_email,user_status:'Active' })

  if (!data) {
    return res.json({ message: "user email is wrong" })
      }
      
       const token = jwt.sign({
        id: data._id
    }, 'secret', { expiresIn: 5 * 60 });
            

            data?data.user_token=token:data.user_token;
            await data.save()
            // forgotPassword(user_email, token)
        return res.json({ success:true,message: 'Please check your email' })

}


