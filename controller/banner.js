const mongoose = require('mongoose');
// const multer = require('multer')
const banner = require('../model/banner')
const path = require('path');
const ErrorHandler = require('../utils/errorHandling')
const asyncCatchHandler = require('../middleware/catchAsyncError');
const machineModel = require('../model/verdering')

exports.addBanner = asyncCatchHandler(async (req, res, next) => {

    const { banner_title, banner_description, banner_status, banner_machine_ids, banner_start_date, banner_end_date } = req.body
    const bannerObj = new banner({
        banner_title,
        banner_description,
        banner_status,
        banner_machine_ids,
        banner_image:req.file.path,
        banner_start_date,
        banner_end_date
    })

    await bannerObj.save();
    return res.json({
        success:true,
        message: 'Banner is save successfully'
    })
    
})

exports.deleteBanner = asyncCatchHandler(async (req, res, next) => {

    const id = req.params.id;


    const banner_data = await banner.findOneAndUpdate({ _id: mongoose.Types.ObjectId(id), banner_status: 'Active' }, { banner_status: "InActive" });

    if (!banner_data) {
        return next(new ErrorHandler('No banner found', 404))
    }
    res.json({
        success: true,
        message: "Banner is in-Active successfully"
    })
}
)

exports.updateBanner = asyncCatchHandler(async (req, res, next) => {
    const id = req.params.id;
    const { banner_title, banner_status, banner_description, banner_machine_ids, banner_start_date, banner_end_date } = req.body
    const profile = await banner.findOne({_id:mongoose.Types.ObjectId(id)})
    if (!profile) {
        return next(new ErrorHandler('No banner found', 404))

    }

    if (!req.file && !banner_title && !banner_status && !banner_description && !banner_machine_ids && !banner_start_date && !banner_end_date) {
        return next(new ErrorHandler('please provide data for update', 404))
    }
    const image = req.file ? req.file.path : "";
    if (image && !image.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
        return next(new ErrorHandler('Only JPG ,PNG and JPEG file format is supported', 404))
    }
    const bannerModel = await banner.findOne({_id:id})

    banner_title ? bannerModel.banner_title = banner_title : bannerModel.banner_title
    banner_status ? bannerModel.banner_status = banner_status : bannerModel.banner_status
    banner_description ? bannerModel.banner_description = banner_description : bannerModel.banner_description;
    banner_machine_ids ? bannerModel.banner_machine_ids = banner_machine_ids : bannerModel.banner_machine_ids;
    image ? bannerModel.banner_image = image : bannerModel.banner_image
    banner_start_date ? bannerModel.banner_start_date = banner_start_date : bannerModel.banner_start_date;
    banner_end_date ? bannerModel.banner_end_date = banner_end_date : bannerModel.banner_end_date
    await bannerModel.save();
    res.json({
        success: true,
        message: "Banner is updated successfully"
    })


})

exports.allBanner = asyncCatchHandler(async (req, res, next) => {
    const allbanner = await banner.aggregate([{
        $unwind: {
            path: '$banner_machine_ids',
            preserveNullAndEmptyArrays: true
        }
    }, {
        $lookup: {
            from: 'machines',
            localField: 'banner_machine_ids',
            foreignField: '_id',
            as: 'Machine'
        }
    }, {
        $unwind: {
            path: '$Machine',
            preserveNullAndEmptyArrays: true
        }
    }, {
        $group: {
            _id: {
                _id: '$_id',
                banner_title: '$banner_title',
                banner_description: '$banner_description',
                banner_image: '$banner_image',
                banner_status: '$banner_status',
                banner_start_date: '$banner_start_date',
                banner_end_date: '$banner_end_date'
            },
            Machines: {
                $push: '$Machine'
            }
        }
    }, {
        $project: {
            _id: '$_id._id',
            banner_title: '$_id.banner_title',
            banner_description: '$_id.banner_description',
            banner_image: '$_id.banner_image',
            banner_status: '$_id.banner_status',
            banner_start_date: '$_id.banner_start_date',
            banner_end_date: '$_id.banner_end_date',
            banner_machine_ids: '$Machines'
        }
    }, {
        $sort: {
            _id: -1
        }
    }])



    if (allbanner.length < 1) {
        return next(new ErrorHandler('No banner found', 404))
    }

return res.json({
        success: true,
        banner: allbanner
    })

})


exports.machineBanner = asyncCatchHandler(async (req, res, next) => {

    const { machine_id } = req.params
    const allbanner = await banner.find({ banner_status: 'Active', $or: [{ banner_machine_ids: { $elemMatch: { $eq: machine_id } } }, { banner_machine_ids: { $size: 0 } }] })
        .select({ 'banner_machine_ids': 0 }).sort({ _id: -1 })


    if (allbanner.length < 1) {
        return next(new ErrorHandler('No banner found', 404))
    }

    res.json({
        success: true,
        banner: allbanner
    })

})


