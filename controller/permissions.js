const ErrorHandler = require("../utils/errorHandling");
// const AsyncAwaitHandling = require("../middleware/catchAsyncError");
const model = require("../model/permissions");
const asyncCatchHandler = require("../middleware/catchAsyncError");
const mongoose = require("mongoose");




exports.permissionsAdd = asyncCatchHandler(async (req, res, next) => {
const permissions=
  [
    {name:'Product',  permission:['product_create','product_view','product_edit','product_delete']},
    {name:'Machine',permission:['machine_create','machine_view','machine_edit','machine_delete'  ]},
    {name:'Channel',permission:['channel_create','channel_view','channel_edit','channel_delete','channel_merge','channel_unmerge','row_add','row_delete']},
    {name:'Canteen',permission:['canteen_create','canteen_view','canteen_edit','canteen_delete']},
    {name:'Discount',permission:['discount_create','discount_view','discount_edit','discount_delete']},
    {name:'Promotion',permission:['promotion_create','promotion_view','promotion_edit','promotion_delete']},
    {name:'User',permission:['user_create','user_view','user_edit','user_delete','white_list_user']},
    {name:'Category',permission:['category_create','category_view','category_edit','category_delete']},
    {name:'Banner',permission:['banner_create','banner_view','banner_edit','banner_delete']},
    {name:'Page_Builder',permission:['page_create','page_view','page_edit','page_delete']},
    {name:'Inventory',permission:['inventory_view','product_add','product_remove']},
    {name:'Permission',permission:['permission_view','permission_create']},
    {name:'Role & Permissions',permission:['role_create','role_edit','role_view','role_delete','role_permission_view']},
 {name:'Waste_Management',permission:['wastage_create','wastage_view','wastage_edit','wastage_delete']}
  ]


const allPermissions=await model.find({}).count()

if(allPermissions<1){
  const add=await new model({
    permissions:permissions
  })
  await add.save()
  return res.json({
    success:true,
    permissions:add
  })
}
return res.json({
  success:false,
  message:'already created'
})

})

exports.displayPermissions = asyncCatchHandler(async (req, res, next) => {
  const Data = await model.find({permission_status:'Active'});

  if (Data.length < 1) {
    return next(new ErrorHandler("Permissions record not found"), 404);
  }

  res.json({
    success: true,
    Data,
  });
});


