const mongoose = require("mongoose");
const promotion = require("../model/promotion");
const { body, validationResult } = require("express-validator");
const promotionModel = require("../model/promotion");
const ErrorHandler = require("../utils/errorHandling");
const asyncCatchHandler = require("../middleware/catchAsyncError");
const {
  promotionUpdate
} = require("../email/emailMessage");
const userModel = require("../model/userModel");

exports.addPromotions = asyncCatchHandler(async (req, res, next) => {

  let {
    promo_name,
    promo_code,
    promo_description,
    promo_start_date,
    promo_end_date,
    promo_type,
    promo_value,
    promo_productid,
    promo_status,
  } = req.body;
  // percentage ? (percentage = Number(percentage)) : "";
  
  const verifyCode = await promotionModel.findOne({ promo_code: promo_code,promo_status:'Active' });

  if (verifyCode) {
   return res.json({
     success:false,
     message:"This promotion code is already registered"
   })
  }
  // const applyInDate = new Date(promo_start_date).getTime() / 1000;
  // const expireInDate = new Date(promo_end_date).getTime() / 1000;
  const model = await new promotionModel({
    promo_name,
    promo_code,
    promo_description,
    promo_value,
    promo_type,
    promo_productid:promo_productid?promo_productid:null,
    promo_status,
    promo_start_date,
    promo_end_date
  });
// console.log("Discounttttttttttttttttttttttttttttt")
  await model.save();
  const whiteList=await userModel.find({user_status:'Active',user_role:"white_list"})
  
  whiteList?whiteList.forEach(data=>{
    promotionUpdate(data.user_name,data.user_email,promo_name,promo_code,promo_value,promo_type)
  }):""
  return res.json({
    success:true,
    message:'Promotion is Added successfully'
  })
 
});

exports.allPromotions = asyncCatchHandler( async (req, res) => {
  const allPromotions = await promotionModel.find({});

  if (allPromotions.length < 1) {
    return res.json({
      message: "No Promotion found",
    });
  }

  res.json({
    allPromotions: allPromotions,
  });
});

exports.deletePromotions = asyncCatchHandler(async (req, res,next) => {
  const id = req.params.id;

 const data=await promotionModel.findOne({_id:mongoose.Types.ObjectId(id),promo_status:'Active'})
 if(!data){
  return next(new ErrorHandler("promo not  found", 404));
 }
 data? data.promo_status='InActive':data.promo_status
 await data.save();
 return res.json({
   success:true,
  message: "Promotion is deleted successfully",
});
  
});

exports.updatePromotions = asyncCatchHandler( async (req, res) => {
  const { id } = req.params;
  const {
    promo_name,

    promo_description,
    promo_start_date,
    promo_end_date,
    promo_type,
    promo_value,
    promo_productid,
    promo_status,
  } = req.body;

  const promotion = await promotionModel.findOne({_id:mongoose.Types.ObjectId(id)});

  if (!promotion) {
    return res.json({
      messsage: "Promotion not found ",
    });
  }
 
 
  let expireInDate = "";
  let applyInDate = "";
  // if (promo_end_date) {
  //   expireInDate = new Date(promo_end_date).getTime() / 1000;
  // }
  // if (promo_start_date) {
  //   applyInDate = new Date(promo_start_date).getTime() / 1000;
  // }
  promo_name ? (promotion.promo_name = promo_name) : promo_name;
  promo_value ? (promotion.promo_value = promo_value) : promo_value;
  promo_productid
    ? (promotion.promo_productid = promo_productid)
    : promo_productid;
  promo_status ? (promotion.promo_status = promo_status) : promo_status;

  promo_description
    ? (promotion.promo_description = promo_description)
    : promo_description;

  promo_type ? (promotion.promo_type = promo_type) : promo_type;
  promo_start_date
    ? (promotion.promo_start_date = promo_start_date)
    : promo_start_date;
  promo_end_date ? (promotion.promo_end_date = promo_end_date) : promo_end_date;

  promotion.save();

  return res.json({
    success: true,
    message: "promotion is updated successfully",
    promotion,
  });
});

const userValidationRules = () => {
  return [
    // username must be an email
    body("username").isEmail(),
    // password must be at least 5 chars long
    body("password").isLength({ min: 5 }),
  ];
};

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(422).json({
    errors: extractedErrors,
  });
};
