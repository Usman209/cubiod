// addWastage

const mongoose = require("mongoose");
const canteenModel=require('../model/canteen')
const productModel = require("../model/product");
const machineModel = require("../model/verdering");
const wastageModel=require('../model/wastage')

const asyncCatchHandler = require("../middleware/catchAsyncError");
const ErrorHandler = require("../utils/errorHandling");

exports.addWastage =asyncCatchHandler (async (req, res, next) => {


const {wastage_canteen_id,wastage_machine_id,wastage_product_id,wastage_product_quantity,wastage_status}=req.body;

const canteen=await canteenModel.findOne({_id:mongoose.Types.ObjectId(wastage_canteen_id),canteen_status:'Active'})
const product=await productModel.findOne({_id:mongoose.Types.ObjectId(wastage_product_id),product_status:'Active'})
const machine=await machineModel.findOne({_id:mongoose.Types.ObjectId(wastage_machine_id),machine_status:'Active'})

if(!canteen){
    return next(new ErrorHandler("Canteen not found", 404));
}
if(!product){
    return next(new ErrorHandler("Product not found", 404));
}
if(!machine){
    return next(new ErrorHandler("Machine not found", 404));
}
if(wastage_product_id<1){
    return next(new ErrorHandler("Product quantity must be greater than 0", 404));   
}
const model=await new wastageModel({
    wastage_canteen_id,
    wastage_machine_id,
    wastage_product_id,
    wastage_product_quantity,
    wastage_status

})
await model.save();

return res.json({
    success:true,
    message:'Wastage product is added successfully'
})
})

exports.allWastage =asyncCatchHandler (async (req, res, next) => {

    const allWastageProduct=await wastageModel.find({}).populate('wastage_canteen_id').populate('wastage_product_id').populate('wastage_machine_id')
    if(allWastageProduct.length<1){
        
        return next(new ErrorHandler("Wastage product not found", 404)); 
    }
    return res.json({
        success:true,
       data:allWastageProduct
    })
    })

exports.deleteWastage =asyncCatchHandler (async (req, res, next) => {
const {wastage_id}=req.params
const deleteWastage=await wastageModel.findOne({_id:mongoose.Types.ObjectId(wastage_id),wastage_status:'Active'})

if(!deleteWastage){
    return next(new ErrorHandler("Wastage product not found", 404)); 
}
deleteWastage?deleteWastage.wastage_status='InActive':deleteWastage.wastage_status

await deleteWastage.save()
        return res.json({
            success:true,
           message:'Wastage Product is deleted successfully'
        })
        })

exports.updateWastage =asyncCatchHandler (async (req, res, next) => {

    const {wastage_id}=req.params;
    const {wastage_canteen_id,wastage_machine_id,wastage_product_id,wastage_product_quantity,wastage_status}=req.body;

    if(!wastage_canteen_id&&!wastage_machine_id&&!wastage_product_id&&!wastage_product_quantity&&!wastage_status){
        return next(new ErrorHandler("Please provide new data for update", 404));  
    }
const canteen=await canteenModel.findOne({_id:mongoose.Types.ObjectId(wastage_canteen_id),canteen_status:'Active'})
const product=await productModel.findOne({_id:mongoose.Types.ObjectId(wastage_product_id),product_status:'Active'})
const machine=await machineModel.findOne({_id:mongoose.Types.ObjectId(wastage_machine_id),machine_status:'Active'})
if(wastage_canteen_id&&!canteen){
    return next(new ErrorHandler("Canteen not found", 404));
}
if(wastage_product_id&&!product){
    return next(new ErrorHandler("Product not found", 404));
}
if(wastage_machine_id&&!machine){
    return next(new ErrorHandler("Machine not found", 404));
}
if(wastage_product_quantity<1){
    return next(new ErrorHandler("Product quantity must be greater than 0", 404));   
}
const updateWastage=await wastageModel.findOne({_id:mongoose.Types.ObjectId(wastage_id)})

if(!updateWastage){
    return next(new ErrorHandler("Wastage product not found", 404)); 
}
wastage_canteen_id?updateWastage.wastage_canteen_id=wastage_canteen_id:updateWastage.wastage_canteen_id
wastage_machine_id?updateWastage.wastage_machine_id=wastage_machine_id:updateWastage.wastage_machine_id
wastage_product_id?updateWastage.wastage_product_id=wastage_product_id:updateWastage.wastage_product_id
wastage_product_quantity?updateWastage.wastage_product_quantity=wastage_product_quantity:updateWastage.wastage_product_quantity
wastage_status?updateWastage.wastage_status=wastage_status:updateWastage.wastage_status

await updateWastage.save()
        return res.json({
            success:true,
           message:'Wastage Product is updated successfully',
           data:updateWastage
        })
        })
       