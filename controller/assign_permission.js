// 

const mongoose = require('mongoose');
// const multer = require('multer')

const ErrorHandler = require('../utils/errorHandling')
const asyncCatchHandler = require('../middleware/catchAsyncError');

const roleModel=require('../model/roles')


exports.assignPermission = asyncCatchHandler(async (req, res, next) => {

    const {role_name,permission_name,role_status}=req.body
    if(!role_name){
        return next(new ErrorHandler('role name must be required', 404))
    }
    const role=await roleModel.findOne({role_name:role_name})
 
   
    if(role){
        return res.json({success:false,message:'role name is already registered'})
    }

const roleAssgin=new roleModel({
    role_name:role_name,
    permission_name:permission_name,
    role_status:role_status
})

await roleAssgin.save()
return res.json({
    success:true,
    message: 'Role is created successfully'
})

    })

exports.updateRolePermission = asyncCatchHandler(async (req, res, next) => {
const {role_id}=req.params
        const {permission_name,role_name,role_status}=req.body

        
        const roleName=await roleModel.findOne({role_name:role_name})
 
  
        if(roleName&&roleName._id!=role_id){
            return res.json({success:false,message:'role name is already registered'})
        }
        const role=await roleModel.findOne({_id:mongoose.Types.ObjectId(role_id)})
     
     
        if(!role){
            return res.json({success:false,message:'role not found'})
        }
    
if(role){
        permission_name?role.permission_name=permission_name:permission_name
        role_name?role.role_name=role_name:role_name
        role_status?role.role_status=role_status:role_status
    
    await role.save()
    return res.json({
        success:true,
        message: 'Role is updated successfully'
    })
}
return res.json({
    success:false,
 
})
        })
    
exports.allRolePermissions= asyncCatchHandler(async (req, res, next) => {

            const {role_id}=req.params
            const role=await roleModel.findOne({_id:mongoose.Types.ObjectId(role_id)})
         
           
            if(!role){
                return next(new ErrorHandler('role not found', 404))
            }
        
            return res.json({
                success:true,
                data:role
            })
            
        })
        
exports.deleteRole = asyncCatchHandler(async (req, res, next) => {

            const { role_id } = req.params;
        
            const deleteData = await roleModel.findOne({_id:mongoose.Types.ObjectId(role_id),role_status:'Active'});
        
            if (!deleteData) {
                return next(new ErrorHandler('Role not found', 404))
            }
            deleteData?deleteData.role_status='InActive':""
            await deleteData.save()
            res.json({
                success: true,
                message: 'Role record is deleted successfully'
            })
        
        })

exports.displayRoles = asyncCatchHandler(async (req, res, next) => {


            const Data = await roleModel.find({});
        
            if (Data.length < 1) {
                return next(new ErrorHandler('Roles record not found',404))
            }
        
            res.json({
                success: true,
                Data
            })
        })

















