const res = require('express/lib/response');
const mongoose = require('mongoose');
const ErrorHandler = require('../utils/errorHandling')
const model = require('../model/verdering');
const channelModel = require('../model/channel')
const asyncCatchHandler = require('../middleware/catchAsyncError')
const canteenModel = require('../model/canteen')
exports.addMachine = asyncCatchHandler(async (req, res, next) => {

  let { machine_name,
    canteen_id,
    machine_code,
    machine_status,
    payment_method,
    machine_location,
    column,
    rows } = req.body;

  rows ? rows = rows : rows = 5
  column ? column = column : column = 7

  const total_channel = rows * column;

  const canteenCheck = await canteenModel.findById({ _id: canteen_id })

  if (!canteenCheck) {
    return next(new ErrorHandler('Canteen not found', 404))
  }

  const machineModel = new model({
    machine_name,
    canteen_id,
    machine_code,
    machine_status,
    payment_method,
    machine_location,
    machine_channel_column: column,
    machine_channel_rows: rows,
  })
  await machineModel.save();
  let machineId = ""
  machineId = machineModel._id



let number=0
let cn=0
  for (let i = 1; i <= rows; i++) {
    number=number+10
cn=number
    for (let j = 1; j <= column; j++) {
      cn=cn+1
      const channelModel11 = await new channelModel({
        machine_id: machineId,
        row_number: i,
        channel_name: cn,
        channel_order:j
      })
      await channelModel11.save();

    }



  }


  res.json({
    success: true,
    message: 'Vending Machine is created successfully',
    machineModel
  })


});

exports.allMachine = asyncCatchHandler(async (req, res, next) => {
  const machine = await model.find({});
  // console.log(machine, "4")
  if (machine && machine.length < 1) {
    return next(new ErrorHandler("No vending machine found", 404));
  }

  res.json({
    success: true,
    machine,
  });
});

exports.listMachine = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;

  const machineModel = await model.findOne({_id:mongoose.Types.ObjectId(id)});

  if (!machineModel) {
    return next(new ErrorHandler('Machine not found', 404))
  }
  res.json({
    success: true,
    machineModel,
  });
});
exports.listMachineByCanteenId = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;

  const machineModel = await model.find({ canteen_id: id });

  if (!machineModel) {
    return res.json({
      success: false,
      message: "Machines not found",
    });
  }
  res.json({
    success: true,
    machineModel,
  });
});

exports.deleteMachine = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;

  const deleteModel = await model.findOneAndUpdate({ _id: mongoose.Types.ObjectId(id),machine_status:'Active' }, { machine_status: "InActive" });
  if (!deleteModel) {
    return next(new ErrorHandler("No vending machine found", 404));
  }
  res.json({
    success: true,
    message: "Machine is deleted successfully",
  });
});


exports.updateMachine = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;
  const { machine_name,
    canteen_id,
    machine_code,
    payment_method,
    machine_location,
    machine_status
  } = req.body;
  const updateMachine = await model.findOne({_id:mongoose.Types.ObjectId(id)});
  if (!updateMachine) {
    return next(new ErrorHandler("No vending machine found", 404));
  }
  if (!canteen_id && !machine_code && !machine_status && !payment_method && !machine_location) {
    return next(
      new ErrorHandler(
        "Machine data must must be required for update",
        404
      )
    );
  }
const canteenVerify=canteen_id?await canteenModel.findOne({_id:mongoose.Types.ObjectId(canteen_id),canteen_status:'Active'}):""
if(canteen_id && !canteenVerify){
  return next(new ErrorHandler("canteen not found found", 404));
}
const updatePayment=payment_method?payment_method.length:""
  canteen_id ? (updateMachine.canteen_id = canteen_id) : canteen_id;
  updatePayment ?updateMachine.payment_method=payment_method : updateMachine.payment_method;
  machine_location ? (updateMachine.machine_location = machine_location) : machine_location;
  machine_status ? (updateMachine.machine_status = machine_status) : machine_status;
  machine_name ? (updateMachine.machine_name = machine_name) : machine_name;
  machine_code ? (updateMachine.machine_code = machine_code) : machine_code;

  updateMachine.save();

  res.json({
    success: true,
    message: "Machine is updated successfully",
    updateMachine,
  });
});



////   Display Machine Product In admin side

exports.allMachineProductAdminSide = asyncCatchHandler(
  async (req, res, next) => {
    const { machine_id } = req.params;

    let product = await model.aggregate([
      {
        $unwind: {
          path: "$channel",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "inventories",
          localField: "channel._id",
          foreignField: "channel_id",
          as: "inventories",
        },
      },
      {
        $lookup: {
          from: "products",
          localField: "channel.assign_product",
          foreignField: "_id",
          as: "product",
        },
      },
      {
        $project: {
          name: 1,
          canteenId: 1,
          Machine_Code: 1,
          status: 1,
          channel: 1,
          inventoryies: 1,
          product: 1,
        },
      },
      {
        $group: {
          _id: {
            _id: "$channel.assign_product",
            machine_name: "$name",
            canteenId: "$canteenId",
            Machine_Code: "$Machine_Code",
          },
          Assigned: {
            $sum: "$channel.quantity",
          },
        },
      },
      {
        $lookup: {
          from: "products",
          localField: "_id._id",
          foreignField: "_id",
          as: "products",
        },
      },
      {
        $match: {
          products: {
            $ne: [],
          },
        },
      },
      {
        $unwind: {
          path: "$products",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: "$_id",
          Assigned: 1,
          products: 1,
          channel: 1,
          TotalProduct: {
            $add: ["$Assigned", "$products.quantity"],
          },
        },
      },
    ]);

    return res.json({
      success: true,
      product,
    });
  }
);

// exports.allMachineProductAdminSide = asyncCatchHandler(async (req, res, next) => {

//     const { machine_id } = req.params;

//     let product = await model.findOne({ _id: mongoose.Types.ObjectId(machine_id) }).populate('canteenId').populate('channel.assign_product')

//     product = product.channel.filter(item => item.assign_product !== null)
//     return res.json({
//         success: true,
//         product
//     })

// })

///  Display Machine Product

exports.displayMachineProduct = asyncCatchHandler(async (req, res, next) => {
  const { machine_id } = req.params;

  let product = await model
    .findById({ _id: machine_id }, { "channel.assign_product.quantity": 0 })
    .populate({
      path: "canteenId",
      select: { _id: 1, name: 1, location: 1, status: 1 },
    })
    .populate({
      path: "channel.assign_product",
      select: {
        _id: 1,
        name: 1,
        price: 1,
        description: 1,
        image: 1,
        recipe: 1,
        allergic: 1,
        VAT: 1,
        expiry_date: 1,
        status: 1,
      },
    });

  return res.json({
    success: true,
    product,
  });
});
