const mongoose = require('mongoose');
const user = require('../model/userModel');
const bcrypt = require('bcryptjs')

const asyncCatchHandler = require('../middleware/catchAsyncError');
const ErrorHandler = require('../utils/errorHandling');
const userModel = require('../model/userModel');
const canteenModel = require('../model/canteen')
const channelModel=require('../model/channel')
const machineModel=require('../model/verdering')







/// Display All Canteens under One Machine Filler 
exports.displayMachineFillerCanteens = asyncCatchHandler(async (req, res, next) => {

    const { machine_filler_id } = req.params

    //   const { canteen_id, supplier_id } = req.params;

    const user = await userModel.findOne({ _id: mongoose.Types.ObjectId(machine_filler_id),user_status:'Active' })
    if (!user) {
        return next(new ErrorHandler("User not found", 404));
    }
    const machineFillerMachines = await canteenModel.find({ machine_filler_id: mongoose.Types.ObjectId(machine_filler_id),canteen_status:'Active' })

    if (machineFillerMachines.length < 1) {
        return next(new ErrorHandler("Machine Filler have no Canteen", 404));
    }


    res.json({
        success: true,
        data: machineFillerMachines
    })
})

//// Display Machine under 1 Canteens


exports.displayMachines = asyncCatchHandler(async (req, res, next) => {

    const { canteen_id,machine_filler_id } = req.params

    //   const { canteen_id, supplier_id } = req.params;

    const user = await userModel.findOne({ _id: mongoose.Types.ObjectId(machine_filler_id),user_status:'Active' })
    if (!user) {
        return next(new ErrorHandler("User not found", 404));
    }
    const machineFillerMachines = await canteenModel.findOne({_id:mongoose.Types.ObjectId(canteen_id), machine_filler_id: mongoose.Types.ObjectId(machine_filler_id),canteen_status:'Active' })

    if (machineFillerMachines < 1) {
        return next(new ErrorHandler("Machine Filler have not assign this Canteen", 404));
    }

const machine=await machineModel.find({canteen_id:mongoose.Types.ObjectId(canteen_id),machine_status:'Active'})

if(machine.length<1){
    return next(new ErrorHandler("No machine Associate with this canteen", 404));
}   
res.json({
        success: true,
        data: machine
    })
})


/// Update inventory in Machine Channel
exports.inventoryUpdateByMachineFiller = asyncCatchHandler(async (req, res, next) => {
    const {machine_id}=req.params
const {channel}=req.body
const allChannel=await channelModel.find({machine_id:mongoose.Types.ObjectId(machine_id)});
let arrayData=[]
let check=false
// Check quantity type is number
channel.map(data=>{
    if(!data.channel_product_quantity|| typeof data.channel_product_quantity =="string"){
         check=true
    }
})
if(check){
    return next(new ErrorHandler("Quantity must be required", 404));
}
//// Check Product is assign to channel or not
// channel.map(data1=>{
// allChannel?allChannel.map(data=>{
   
// if(data._id==data1.channel_id){
// if(data.channel_product_id==null){
 
//     arrayData.push(data)
// }
// }
// }):""})
// if(arrayData&&arrayData.length>=1){
//     return res.json({
//         success:false,
//         message:"These Channel don't have assign product.Please assign product to update inventory",
//         data:arrayData
//     })
// }
///

/// Update Inventory
channel?channel.map(async data=>{
const channelUpdate=await channelModel.findOne({machine_id:mongoose.Types.ObjectId(machine_id),_id:mongoose.Types.ObjectId(data.channel_id)})
channelUpdate?channelUpdate.channel_product_quantity=data.channel_product_quantity:data.channel_product_quantity
channelUpdate?await channelUpdate.save():""


}):""

   return res.json({
        success: true,
        message:"Inventory is updated successfully"
    })
})


