
const mongoose = require('mongoose');
const order = require('../model/order');
const product = require('../model/product')
const ErrorHandler = require('../utils/errorHandling')
const asyncCatchError = require('../middleware/catchAsyncError')
const discount = require('../model/discount')
const productModel = require('../model/product')
const promotionModel = require('../model/promotion');
const inventory = require( '../model/inventory' );
const sgMail = require("@sendgrid/mail");
// sgMail.setApiKey(process.env.SENDGRID_API_KEY);

// const fs = require("fs");

// pathToAttachment = `${__dirname}/files/`;
// attachment = fs.readFileSync(pathToAttachment).toString("base64");




exports.addOrder = asyncCatchError(async (req, res, next) => {
  // check if product is in stock
  // calculate total price of all products in cart
  // console.log("Req User", req.user)
  var { user_id, product, promoCode } = req.body;
  var idd = [];
  var machineId = []
  var inventoryId = []
  var productQuantity = [];
  var productExtra = []
  const productData = product

  if (!product || product.length < 1) {
    return next(new ErrorHandler('Order must have atleat one Product'))
  }
  product.forEach(data => {
    idd.push(data.product_id)
    productQuantity.push(data.quantity)
    inventoryId.push({ id: data.product_id, quantity: data.quantity })
    machineId.push(data.machine_id)
  });

  var error = false
  var products = []
  
// * invernttoy out of stock :

  // const inventoryData1 = await inventory.find({ 'productId': { $in: idd } }).populate('productId')

  // inventoryData1.forEach(data => {
  //   product.forEach(data1 => {
  //     if (data.quantity < data1.quantity) {
  //       error = true
  //     }
  //   })
  // })
  // if (error) {
  //   return next(new ErrorHandler('Product is out of stock'))
  // }

  var productModelData = await productModel.find({}) 
  var count = 0
  var quantityCheck = false
  product.forEach(data => {
    if (data.quantity < 1) {
      quantityCheck = true
    }
  })
  if (quantityCheck) {
    return next(new ErrorHandler('Product must have atleat one Quantity'))
  }

// * promo code  
  // const promoData = await promotionModel
  //   .findOne({
  //     // 20             14
  //     code: promoCode, applyIn: { $lte: (new Date().getTime() / 1000) }, expireIn: {
  //       $lte: (new Date().getTime() / 1000)
  //     }
  //   });

  // if (promoData) {
  //   promocode = promoData.percentage
  // }
  // else { promocode = 0 }

// * discount  :

  // const discountOr = await discount.find({
  //   'discount_productId': { $in: idd },
  // }).populate('discount_productId')
  
  // console.log(discountOr, "discountttttttttttttttttt")
  var i = 0
  var discountCo = []
  var product_id = []
  var total = 0
  var totalAccount = 0;
  var count = 0

  // if (req.user && req.user.role == 'white_user') {
  //   discountOr.forEach(data1 => {
  //     product.forEach(data => {
        
  //       console.log(data1.discount)
        
  //       product_id.push(data.product_id)

  //       if (data1.product_Id._id == data.product_id) {
  //         // console.log(data1.product_Id.price, "Data1", data1.discount)
  //         // console.log(data1.product_Id.allergic, "boss")
  //         // console.log(data1.product_Id, "IDdd")
  //         if (data && data.quantity < 1) {

  //           return next(new ErrorHandler('product must have atleast one quantity', 404))
  //         }

  //         if (data1.discount_type == 'Cuboid') {
  //           productExtra.push({
  //             product_id: data.product_id,
  //             name: data1.product_Id.name,
  //             price: data1.product_Id.price,
  //             quantity: data.quantity,
  //             discount: data1.discount_Percentage
  //           })

  //           total = (Number(data1.product_Id.price) - (Number(data1.product_Id.price) * (Number(data1.discount_Percentage) / 100))) * Number(data.quantity)
  //           discountCo.push({ "product_id": data.product_id, "price": Number(data.price), 'quantity': Number(data.quantity), 'total': Number(total), 'dscount': Number(data1.discount_Percentage) });
  //           total = Number(count) + Number(total)
  //           totalAccount = Number(totalAccount) + Number(total)
  //           product = product.filter(item => item.product_id !== data.product_id);
  //         }

  //         // console.log((data1.expireIn >= (new Date().getTime() / 10000)), (data1.applyIn >= (new Date().getTime() / 10000)), "Cool")
  //         if (data1.discount_type == 'Machine' && data1.machine_id == data.machine_id && data1.expireIn >= (new Date().getTime() / 10000) && data1.applyIn >= (new Date().getTime() / 10000)) {
  //           // 26                      25
  //           productExtra.push({
  //             product_id: data.product_id,
  //             name: data1.product_Id.name,
  //             price: data1.product_Id.price,
  //             quantity: data.quantity,
  //             discount: data1.discount_Percentage
  //           })
  //           total = (Number(data1.product_Id.price) - (Number(data1.product_Id.price) * (Number(data1.discount_Percentage) / 100))) * Number(data.quantity)

  //           discountCo.push({ "product_id": data.product_id, "price": Number(data.price), 'quantity': Number(data.quantity), 'total': Number(total), 'dscount': Number(data1.discount_Percentage) });

  //           total = Number(count) + Number(total)
  //           totalAccount = Number(totalAccount) + Number(total)


  //           product = product.filter(item => item.product_id !== data.product_id);

  //         }


  //       }


  //     })


  //   })
  // }

console.log('here is data  : ', product.length)
  product.forEach(data => {
    console.log('ok', data)
    productModelData.forEach(data1 => {

      if (data.product_id == data1._id) {
        productExtra.push({
          product_id: data.product_id,
          name: data1.name,
          price: data1.price,
          quantity: data.quantity,
          discount: 0
        })
       
         var productTotal =Number(data1.price) * Number(data.quantity);
          // productExtra.push({
          //   productTotal:productTotal
          // })
        
        productModelData = productModelData.filter(item => item.product_id != data.product_id);
         console.log('totals1   :', productModelData)
        totalAccount = totalAccount + Number(data1.price) * Number(data.quantity)
        console.log('totals101   :', totalAccount)
      }
    })
    console.log('end')
  })

  console.log('here 178 : ', productExtra)

  if (productModelData.length <= 1) {
    return next(new ErrorHandler('Product not found', 404))
  }
  var i = 0

  // inventoryId.forEach(async data => {
  //   const newpro = await inventory.findOne({ productId: mongoose.Types.ObjectId(data.id) });
  //   if (newpro && newpro.quantity < data.quantity) {
  //     return next(new ErrorHandler('Product is out of stock', 404))
  //   }
  // })
  // console.log('promocode : ',promocode)
  // totalAccount = (Number(totalAccount) - (Number(totalAccount) * (Number(promocode) / 100)))
  // var inventoryData = []
  // inventoryId.forEach(async data => {
  //   inventoryData = await inventory.updateMany({ productId: mongoose.Types.ObjectId(data.id), quantity: { $gte: data.quantity } },
  //     {
  //       $inc: {
  //         quantity: -Number(data.quantity)
  //       }
  //     }, { new: true, runValidators: true, useFindAndModify: true })

  // })


  const orderModel = new order({
    user_id,
    name: req.user.name,
     product: productExtra,
    //  product: productData,
    totalPrice: totalAccount
  })

  data=await orderModel.save();

  await invoicegenerate(data, totalAccount);
  res.json({
    success: true,
    productExtra: productExtra,
    message: "Order is added successfully",
    orderModel,
    totalAccount
  })
})

exports.alladminSideOrder = asyncCatchError(async (req, res, next) => {

  const orderData = await order.find({})
  if (orderData.length < 1) {
    return next(new ErrorHandler('no user order found', 404))
  }
  return res.json({
    success: true,
    orderData
  })
})


exports.userOrders = asyncCatchError(async (req, res, next) => {
  const { id } = req.params;
  const userOrder = await order.find({ user_id: mongoose.Types.ObjectId(id) })
  // console.log(userOrder)
  if (userOrder.length < 1) {
    return next(new ErrorHandler('no user order found', 404))
  }
  
  res.json({
    success: true,
    userOrder
  })
})

exports.deleteOrder = asyncCatchError(async (req, res, next) => {
  const { id } = req.params;
  const deleteOrder = await order.findByIdAndDelete(id);
  if (!deleteOrder) {
    return next(new ErrorHandler('No order found'))
  }
  res.json({
    success: true,
    message: 'Order is Deleted successfully'
  })
})

// const data = {};

const invoicegenerate = async (obj, totalAccount) => {

  const pdfKit = require('pdfkit');
  const fs = require('fs');

  // let companyLogo = "welcome";
  let fileName = './files/Devbatch-invoice.pdf';
  let fontNormal = 'Helvetica';
  let fontBold = 'Helvetica-Bold';

  let sellerInfo = {
    "companyName": "Cuboid",
    "address": "xxxxx",
    "city": "xxxxx",
    "state": "xxxxx",
    "pincode": "xxxxx",
    "country": "Norway",
    "contactNo": "+923001234567"
  }

  let customerInfo = {
    "customerName": "Customer ABC",
    "address": "R783, Rose Apartments, Santacruz (E)",
    "city": "Lahore",
    "state": "Punjab",
    "pincode": "54000",
    "country": "Pakistan",
    "contactNo": "+9200000000"
  }

  let orderInfo = {
    "orderNo": "15484659",
    "invoiceNo": "MH-MU-1077",
    "invoiceDate": "11/05/2021",
    "invoiceTime": "10:57:00 PM",
    "products": [
      {
        "id": "15785",
        "name": "Acer Aspire E573",
        "company": "Acer",
        "unitPrice": 39999,
        "totalPrice": 39999,
        "qty": 1
      },
      {
        "id": "15786",
        "name": "Dell Magic Mouse WQ1545",
        "company": "Dell",
        "unitPrice": 2999,
        "totalPrice": 5998,
        "qty": 2
      }
    ],
    "totalValue": 45997
  }

  function createPdf() {
    try {
      let pdfDoc = new pdfKit();

      let stream = fs.createWriteStream(fileName);
      pdfDoc.pipe(stream);

      pdfDoc.text(
        "PDF Invoice ",
        5,
        5,
        { align: "center", width: 600 }
      );
      // pdfDoc.image(companyLogo, 25, 20, { width: 50, height: 50 });
      pdfDoc.font(fontBold).text("CUBOID", 7, 75);
      pdfDoc
        .font(fontNormal)
        .fontSize(14)
        .text("Order Invoice/Bill Receipt", 400, 30, { width: 200 });
      pdfDoc.fontSize(10).text("", 400, 46, { width: 200 });

      pdfDoc.font(fontBold).text("Sold by:", 7, 100);
      pdfDoc
        .font(fontNormal)
        .text(sellerInfo.companyName, 7, 115, { width: 250 });
      // pdfDoc.text(sellerInfo.address, 7, 130, { width: 250 });
      // pdfDoc.text(sellerInfo.city + " " + sellerInfo.pincode, 7, 145, {
      //   width: 250,
      // });
      // pdfDoc.text(sellerInfo.state + " " + sellerInfo.country, 7, 160, {
      //   width: 250,
      // });

      pdfDoc.font(fontBold).text("Customer details:", 400, 100);
      pdfDoc
        .font(fontNormal)
        .text('User Name : '+obj.name, 400, 115, { width: 250 });
      pdfDoc.text('User ID :'+obj.user_id, 400, 130, { width: 250 });
      // pdfDoc.text(customerInfo.city + " " + customerInfo.pincode, 400, 145, {
      //   width: 250,
      // });
      // pdfDoc.text(customerInfo.state + " " + customerInfo.country, 400, 160, {
      //   width: 250,
      // });

      pdfDoc.text("Order No:" + obj._id, 7, 195, { width: 250 });

      date =new  Date(obj.createdAt)
       console.log(" . code . ",date.getDate() + '-' + date.getMonth() + 1 +'-'+ date.getFullYear() + ' '+date.getHours() + ':'+ date.getMinutes() )

      // pdfDoc.text("Invoice No:" + orderInfo.invoiceNo, 7, 210, { width: 250 });
      pdfDoc.text(
        "Date:" +
          " " +
          date.getDate() +
          "-" +
          date.getMonth() +
          1 +
          "-" +
          date.getFullYear() +
          " " +
          date.getHours() +
          ":" +
          date.getMinutes(),
        7,
        225,
        { width: 250 }
      );

      pdfDoc.rect(7, 250, 560, 20).fill("#FC427B").stroke("#FC427B");
      pdfDoc.fillColor("#fff").text("ID", 20, 256, { width: 90 });
      pdfDoc.text("Product", 190, 256, { width: 190 });
      pdfDoc.text("Qty", 300, 256, { width: 100 });
      pdfDoc.text("Price", 400, 256, { width: 100 });
      pdfDoc.text("Product Total", 500, 256, { width: 100 });

      let productNo = 1;
      obj.product &&
        obj.product.forEach((element) => {
          console.log("adding", element.name);
          let y = 256 + productNo * 20;
          pdfDoc
            .fillColor("#000")
            .text(element.product_id, 20, y, { width: 90 });
          pdfDoc.text(element.name, 190, y, { width: 190 });
          pdfDoc.text(element.quantity, 300, y, { width: 100 });
          pdfDoc.text(element.price, 400, y, { width: 100 });
          pdfDoc.text(element.quantity * element.price, 500, y, { width: 100 });
          productNo++;
        });

      pdfDoc
        .rect(7, 256 + productNo * 20, 560, 0.2)
        .fillColor("#000")
        .stroke("#000");
      productNo++;

      pdfDoc.font(fontBold).text("Total:", 400, 256 + productNo * 17);
      pdfDoc.font(fontBold).text(totalAccount, 500, 256 + productNo * 17);

      pdfDoc.end();
      console.log("pdf generate successfully");
    } catch (error) {
      console.log("Error occurred", error);
    }
  }

  createPdf();

}





// const msg = {
//   to: "test@example.com",
//   from: "test@example.com",
//   subject: "Sending with SendGrid is Fun",
//   text: "and easy to do anywhere, even with Node.js",
//   attachments: [
//     {
//       content: attachment,
//       filename: "attachment.pdf",
//       type: "application/pdf",
//       disposition: "attachment",
//     },
//   ],
// };

// sgMail.send(msg).catch((err) => {
//   console.log(err);
// });