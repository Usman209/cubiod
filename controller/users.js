const mongoose = require("mongoose");
const user = require("../model/userModel");
const role = require("../model/roles");
const bcrypt = require("bcryptjs");

var ObjectId = require("mongodb").ObjectID;
const asyncCatchHandler = require("../middleware/catchAsyncError");
const ErrorHandler = require("../utils/errorHandling");

const {
  userAddMessage,
  userChangePasswordMessage,
} = require("../email/emailMessage");

exports.addUser =asyncCatchHandler (async (req, res, next) => {
  // return res.status(200).json({ 'message': 'Success' })

  var {
    user_name,
    user_email,
    user_role_id,
    user_password,
    user_role,
    user_phone,
    user_status,
    user_permission,
    white_list_user
  } = req.body;
 


  const searchData = await role.findOne({
    _id: mongoose.Types.ObjectId(user_role_id),
    role_status:'Active'
  });
  if(!searchData){
    return next(new ErrorHandler("role not found", 200));
  }

  if (searchData&&searchData.role_name) {
    user_role=searchData.role_name
    } 


 
    const dublicate = await user.find({ user_email: user_email });

    if (dublicate.length > 0) {
      return next(new ErrorHandler("Email is already registered", 200));
    } else {
      const hash = await bcrypt.hash(user_password, 10);
      const model = new user({
        user_name,
        user_email,
        user_password: hash,
        user_role_id,
        user_role: user_role ? user_role : "user",
        user_phone,
        user_permission: user_permission,
        user_status: user_status,
        white_list_user:white_list_user
      });

      await model.save();
      // Enable for Email Send
       userAddMessage(user_name, user_email)
      res.json({
        success: true,
        message: "user is added successfully",
        user: model,
      });
    }
  
});

exports.allUser = asyncCatchHandler(async (req, res, next) => {
  const allUser = await user
    .find()
    .select({ user_token:0,user_password: 0, user_permission: 0 });
  if (allUser.length < 1) {
    return next(new ErrorHandler("user not found", 404));
  }
  res.json({
    success: true,
    allUser: allUser,
  });
});

exports.deleteUser = asyncCatchHandler(async (req, res, next) => {
  const id = req.params.id;
  const usersData = await user.findOne({
    _id: mongoose.Types.ObjectId(id),
    user_status: "Active",
  });

  if (!usersData) {
    return next(new ErrorHandler("user not found", 404));
  }

  usersData ? (usersData.user_status = "InActive") : usersData.user_status;
  await usersData.save();
  return res.json({
    success: true,
    message: "user is deleted successfully",
  });
});

exports.updateUser = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;
  console.log(id);
  var {
    user_name,
    user_role_id,
    user_phone,
    user_status,
    user_permission,
    white_list_user
  } = req.body;


  if (
    !user_name &&
    !user_phone &&
    !user_role_id && !user_status && !white_list_user
  ) {
    return next(new ErrorHandler("Please add new data for update", 200));
  }
  // return res.json({ body: req.body })
  const users = await user.findOne({ _id: mongoose.Types.ObjectId(id)});

  if (!users) {
    return next(new ErrorHandler("user not found", 404));
  }
  
let user_role=""
 
  user_name ? (users.user_name = user_name) : user_name;

  user_phone ? (users.user_phone = Number(user_phone)) : user_phone;
  user_role_id ? (users.user_role_id = user_role_id) : user_role_id;
  user_status ? (users.user_status = user_status) : user_status;
  white_list_user?users.white_list_user=white_list_user:white_list_user
  
  const searchData =user_role_id? await role.findOne({
    _id: mongoose.Types.ObjectId(user_role_id),
    role_status:'Active'
  }):""

  if (user_role_id&&!searchData) {
    return next(new ErrorHandler("role not found", 404));
  }
  if (searchData&&searchData.role_name) {
    user_role=searchData.role_name
  
  } 

  user_permission ? (users.user_permission = user_permission) : user_permission;
  user_role && searchData && searchData.role_name? (users.user_role = user_role) :user_role;
  
  
  const userModelData = await users.save();

  ////// Passwor Change Email
 
  //  userChangePasswordMessage(userModelData.name, userModelData.email)
  /////////////////

  res.json({
    success: true,
    message: "user is updated successfully",
    data: userModelData,
  });
});

exports.userProfile = asyncCatchHandler(async (req, res, next) => {
  const id = req.params.id;
  if (!req.file) {
    return next(
      new ErrorHandler("Image must be required to update profile", 404)
    );
  }
  const image = req.file.path;
  const users = await user.findOne({
    _id: mongoose.Types.ObjectId(id),
    user_status: "Active",
  });

  if (!users) {
    return next(new ErrorHandler("user not found", 404));
  }

  image ? (users.user_profile = image) : image;

  users.save().then((data) => {
    res.json({
      success: true,
      message: "user profile is updated successfully",
    });
  });
});

exports.deleteProfile = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;

  user.findByIdAndDelete(id).then((user) => {
    if (!user) {
      return next(new ErrorHandler("user not found", 404));
    }
    res.json({
      success: true,
      message: "user profile deleted successfully",
    });
  });
});
exports.showProfile = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;
  const userProfile = await user
    .findById({ _id: mongoose.Types.ObjectId(id), user_status: "Active" })
    .select("_id user_name user_profile");

  // console.log(userProfile, "profile")
  if (!userProfile) {
    return next(new ErrorHandler("user not found", 404));
  }
  res.json({
    user_profile: userProfile.user_profile,
  });
});

exports.canteenAdminUsers = asyncCatchHandler(async (req, res, next) => {
  const canteenAdminUsers = await user
    .find({ user_role: "canteen_admin", user_status: "Active" })
    .select(
      "user_name user_email user_phone user_role user_profile user_status"
    );
  if (canteenAdminUsers&& canteenAdminUsers.length<1) {
    return next(new ErrorHandler("user not found", 404));
  }
  res.json({
    success: true,
    data: canteenAdminUsers,
  });
});


exports.supplierUser = asyncCatchHandler(async (req, res, next) => {
  const allUser = await user
    .find({ user_role: "food_supplier", user_status: "Active" })
    .select(
      "user_name user_email user_phone user_role user_profile user_status"
    );
  if (allUser.length < 1) {
    return next(new ErrorHandler("user not found", 404));
  }
  res.json({
    success: true,
    allUser: allUser,
  });
});

exports.whiteListUser = asyncCatchHandler(async (req, res, next) => {
  const allUser = await user
    .find({ user_role: "white_list", user_status: "Active" })
    .select(
      "user_name user_email user_phone user_role user_profile user_status"
    );
  if (allUser.length < 1) {
    return next(new ErrorHandler("user not found", 404));
  }
  res.json({
    success: true,
    allUser: allUser,
  });
});
