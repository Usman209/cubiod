const mongoose = require('mongoose');

const catagoriesModel = require('../model/catagories');
const ErrorHandler = require('../utils/errorHandling');
const productModel=require('../model/product')
const asyncCatchHandler = require('../middleware/catchAsyncError')


exports.addCatagories = asyncCatchHandler(async (req, res, next) => {
    
    const {catagories_name , catagories_status  } = req.body;
    
    const model = await new catagoriesModel({

        catagories_name,
        catagories_image:req.file.path,
        catagories_status
    });
    await model.save()

   return res.status(200).json({
        success: true,
        message: 'Product Catagories is uploaded successfully',
        
    });


})

exports.allCatagories = asyncCatchHandler(async (req, res, next) => {

    const allCatagory = await catagoriesModel.find({})

    if (allCatagory.length < 1) {
        return next(new ErrorHandler('Catagories not  found', 404))
    }

    res.json({
        success: true,
        allCatagory: allCatagory
    })
})

exports.deleteCatagories = asyncCatchHandler(async (req, res, next) => {
    const id = req.params.id;
    catagoriesModel.findOneAndUpdate({ _id: mongoose.Types.ObjectId(id), catagories_status: 'Active' }, { catagories_status: 'InActive' }).then(data => {
        // console.log("data", data)
        if (!data) {
            return next(new ErrorHandler('Catagorie not  found', 404))

        }
        res.json({
            success: true,
            message: "Catagorie is deleted successfully"
        })
    })

})

exports.updateCatagories = asyncCatchHandler(async (req, res, next) => {
    const { id } = req.params
    const { catagories_status, catagories_name } = req.body
    const product = await catagoriesModel.findOne({_id:mongoose.Types.ObjectId(id)})
    if (!product) {
        return next(new ErrorHandler('Catagorie not found', 404))
    }
    if (!catagories_name && !catagories_status && !req.file) {
        return next(new ErrorHandler('New date must be required for update', 404))

    }
    const image = req.file ? req.file.path : "";
    if (image && !image.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
        return next(new ErrorHandler('Only JPG ,PNG and JPEG file format is supported', 404))
    }
    catagories_name ? product.catagories_name = catagories_name : catagories_name
    catagories_status ? product.catagories_status = catagories_status : catagories_status
    image ? product.catagories_image = image : product.catagories_image
    product.save()
    res.json({
        success: true,
        message: 'Category is updated successfully',
        Catagories: product
    })


})

exports.productCatagories = asyncCatchHandler(async (req, res, next) => {
    const { product_catagory_id } = req.params
    const categoryVerify = await catagoriesModel.findOne({_id:mongoose.Types.ObjectId(product_catagory_id),catagories_status:'Active'})
    if (!categoryVerify) {
        return next(new ErrorHandler('Category not found', 404))
    }
    const product = await productModel.find({product_catagory_id:mongoose.Types.ObjectId(product_catagory_id),product_status:'Active'})
    if (product.length<1) {
        return next(new ErrorHandler('Product not found', 404))
    }

  return res.json({
        success: true,
        data: product
    })


})