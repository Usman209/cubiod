const mongoose = require("mongoose");
const catagoryModel=require('../model/catagories')
const model = require("../model/product");

const asyncCatchHandler = require("../middleware/catchAsyncError");
const ErrorHandler = require("../utils/errorHandling");

exports.addProduct = asyncCatchHandler(async (req, res, next) => {

  const {
    product_name,
    product_price,
    product_description,
    product_VAT,
    product_catagory_id,
    product_expiry_date,
    product_status,
    product_gradient
  } = req.body;

 

const verifyCatagories=await catagoryModel.findOne({_id:mongoose.Types.ObjectId(product_catagory_id),catagories_status:'Active'})

if (!verifyCatagories) {
  return next(new ErrorHandler("Catagory not found", 404));
}

const productData = await new model({
    product_name,
    product_price,
    product_description,
    product_VAT,
    product_expiry_date,
    product_catagory_id,
    product_status,
    product_image: req.file ? req.file.path : null,
    product_gradient
  });

  await productData.save();
  return res.json({
    success:true,
    message:'Product is added successfully',
    data:productData
  })

});

exports.allProduct = asyncCatchHandler(async (req, res, next) => {
  const product = await model.find({}).populate('product_catagory_id');

  if (product.length < 1) {
    return next(new ErrorHandler("product not found", 404));
  }
  return res.json({
    success: true,
    product,
  });
});

exports.deleteProduct = asyncCatchHandler(async (req, res, next) => {
  const id = req.params.id;

  const product = await model.findOneAndUpdate({ _id: mongoose.Types.ObjectId(id), product_status: 'Active' }, {
    product_status: 'InActive'
  });

  if (!product) {
    return next(new ErrorHandler("product not found", 404));
  }

  res.json({
    success: true,
    message: "Product is Deleted Successfully",
  });
});
exports.updateProduct = asyncCatchHandler(async (req, res, next) => {
  const id = req.params.id;
  const {
    product_name,
    product_price,
    product_description,
    product_gradient,
    product_VAT,
    product_catagory_id,
    product_expiry_date,
    product_status,
  } = req.body;

  const product = await model.findOne({_id:mongoose.Types.ObjectId(id)});
  if (!product) {
    return next(new ErrorHandler("product not found", 404));
  }
if(product_description&&product_description.length>300){
  return next(new ErrorHandler("Product description must be less than 300 character", 404));
}
const product_gradient_update=product_gradient&&product_gradient.length>0 ?product_gradient:null
  product_name ? (product.product_name = product_name) : product_name;
  product_price ? (product.product_price = product_price) : product_price;
  product_description ? (product.product_description = product_description) : product_description;
  product_gradient_update?product.product_gradient=product_gradient:product.product_gradient
  req.file ? (product.product_image = req.file.path) : product.product_image;
  product_VAT ? (product.product_VAT = product_VAT) : product_VAT;
  product_catagory_id ? (product.product_catagory_id = product_catagory_id) : product_catagory_id;
  product_expiry_date ? (product.product_expiry_date = product_expiry_date) : product_expiry_date;
  product_status ? product.product_status = product_status : product_status;


  await product.save();

  return res.json({
    success: true,
    message: "product is updated successfully",
    product,
  });
});

// Notice
exports.addImage = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;
  let fileArray = [];
  req.files.forEach((element) => {
    // const file = element.originalname

    fileArray.push(element.originalname);
  });
  // console.log(fileArray, "FileArray")
  const image = req.files.originalname;
  const productModel = await model.findByIdAndUpdate(id, {
    image: fileArray,
  });

  productModel.save();

  res.json({
    data: req.files,
    imgData: fileArray,
  });
});
