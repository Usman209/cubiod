const mongoose = require("mongoose");

const canteenModel = require("../model/canteen");
const machineModel = require("../model/verdering");
const channelModel = require("../model/channel");
const ErrorHandler = require("../utils/errorHandling");
const asyncCatchHandler = require("../middleware/catchAsyncError");
const channel = require("../model/channel");

const userModel = require("../model/userModel");
exports.addCanteen = asyncCatchHandler(async (req, res, next) => {

  let { canteen_name, canteen_location, canteen_status,food_supplier_id, canteen_admin_id } = req.body;
 let rows = 5
 let column = 7
 
  const canteenAdminVerify=await userModel.findOne({_id:mongoose.Types.ObjectId(canteen_admin_id),user_role:'canteen_admin',user_status:'Active'})
  if (!canteenAdminVerify) {
    return next(new ErrorHandler("Canteen admin not found", 404));
  }
  const foodSupplier=await userModel.findOne({_id:mongoose.Types.ObjectId(food_supplier_id),user_role:'food_supplier',user_status:'Active'})
  if (!foodSupplier) {
    return next(new ErrorHandler("Food Supplier not found", 404));
  }
  const model = await new canteenModel({
    canteen_name,
    canteen_location,
    canteen_status,
    food_supplier_id,
    canteen_admin_id
  });
  await model.save();

  const pk = model._id;

  var machine_id = "";
  for (let i = 0; i <= 1; i++) {
 
    const model1 = await new machineModel({
      machine_name: canteen_name +' machine' + i,
      canteen_id: pk,
      machine_code: canteen_name + i,
      machine_location: canteen_location,
      machine_channel_rows: rows,
      machine_channel_column: column,
      machine_status:canteen_status
    });
    await model1.save();
    machine_id = model1._id;

    var modeClame = [];
let name=0
    for (let i = 1; i <= rows; i++) {
      name=name+10

      for (let j = 1; j <= column; j++) {
        
        const channelModel11 = await new channelModel({
          machine_id: model1._id,
          row_number: i,
          channel_name: name+j,
          channel_order:j
        })
        await channelModel11.save();
      }
    }


  }
  // console.log(modeClame)

  res.status(200).json({
    success: true,
    message: "Canteen is created successfully",
    canteen: model,
  });
});

exports.allCanteen = asyncCatchHandler(async (req, res, next) => {
  const allCanteen = await canteenModel.find()
    .populate({ path: 'food_supplier_id', select: {user_token:0,user_password:0,user_permission:0 } })
    .populate({ path: 'canteen_admin_id', select: {user_token:0,user_password:0,user_permission:0 } });

  if (allCanteen.length < 1) {
    return next(new ErrorHandler("Canteen not  found", 404));
  }

  res.json({
    success: true,
    allCanteen: allCanteen,
  });
});

exports.deleteCanteen = asyncCatchHandler(async (req, res, next) => {
  const id = req.params.id;

const checkCanteen=await canteenModel.findOne({_id:mongoose.Types.ObjectId(id),canteen_status:'Active'})

if(!checkCanteen){
  return next(new ErrorHandler("Canteen not found", 404));
}
checkCanteen?checkCanteen.canteen_status='InActive':checkCanteen.canteen_status
await checkCanteen.save()
await machineModel.updateMany({canteen_id:mongoose.Types.ObjectId(id)},{machine_status:'InActive'})
return res.json({
  success: true,
  message: "Canteen is deleted successfully",

});

})
exports.searchCanteen = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;
  const searchData = await canteenModel.findOne({ _id: mongoose.Types.ObjectId(id)});
  if (!searchData) {
    return next(new ErrorHandler("Canteen not found", 404));
  }
  res.json({
    success: true,
    searchData,
  });
});
exports.updateCanteen = asyncCatchHandler(async (req, res, next) => {
  const { id } = req.params;
  let { canteen_name, canteen_location, canteen_status,canteen_admin_id,food_supplier_id } = req.body;

let canteen=await canteenModel.findOne({_id:mongoose.Types.ObjectId(id)})
if(!canteen){
  return next(new ErrorHandler("Canteen not found", 404));
}
if(!canteen_admin_id && !food_supplier_id && !canteen_name && ! canteen_location && !canteen_status){
  return next(new ErrorHandler("Canteen new data must be required for update", 404));
}
const canteenAdminVerify=canteen_admin_id?await userModel.findOne({_id:mongoose.Types.ObjectId(canteen_admin_id),user_role:'canteen_admin',user_status:'Active'}):""
if (canteen_admin_id&&!canteenAdminVerify) {
  return next(new ErrorHandler("Canteen admin not found", 404));
}
const foodSupplier=food_supplier_id? await userModel.findOne({_id:mongoose.Types.ObjectId(food_supplier_id),user_role:'food_supplier',user_status:'Active'}) :""
if (food_supplier_id&&!foodSupplier) {
  return next(new ErrorHandler("Food Supplier not found", 404));
}
  canteen_name ? (canteen.canteen_name = canteen_name) : canteen_name;
  canteen_location ? (canteen.canteen_location = canteen_location) : canteen_location;
  canteen_status ? (canteen.canteen_status = canteen_status) : canteen_status;
  canteen_admin_id ? (canteen.canteen_admin_id = canteen_admin_id) : canteen_admin_id;
  food_supplier_id ? (canteen.food_supplier_id = food_supplier_id) : food_supplier_id;

  canteen.save();
  if(canteen_status){
    await machineModel.updateMany({canteen_id:mongoose.Types.ObjectId(id)},{machine_status:canteen_status})
  }
  res.json({
    success: true,
    message: "Canteen is updated successfully",
    canteen: canteen,
  });
});

// Assign Canteen tu canteen_admin
exports.canteenAdmin = asyncCatchHandler(async (req, res, next) => {

  const { canteen_id, canteen_admin_id } = req.params;

  const user = await userModel.findOne({ _id: mongoose.Types.ObjectId(canteen_admin_id), user_role: 'canteen_admin', user_status: 'Active' })
 
  if (!user) {
      return next(new ErrorHandler("Canteen admin not found", 404));
  }
 
  const assignCanteen = await canteenModel.findOne({ _id: mongoose.Types.ObjectId(canteen_id), canteen_status: 'Active' })

  if (!assignCanteen) {
      return next(new ErrorHandler("Canteen not found", 404));
  }

  user ? assignCanteen.canteen_admin_id = canteen_admin_id : assignCanteen.canteen_admin_id

  await assignCanteen.save()

  res.json({
      success: true,
      message: 'canteen is assigned successfully'
  })
})

/// Update Canteen Admins

exports.updatecanteenAdmin = asyncCatchHandler(async (req, res, next) => {

  const { canteen_id, canteen_admin_id } = req.params;

  const canteen_admin = await userModel.findOne({ _id: mongoose.Types.ObjectId(canteen_admin_id), user_role: 'canteen_admin', user_status: 'Active' })
 
  if (!canteen_admin) {
      return next(new ErrorHandler("Canteen admin not found", 404));
  }
 
  const assignCanteen = await canteenModel.findOne({ _id: mongoose.Types.ObjectId(canteen_id), canteen_status: 'Active' })

  if (!assignCanteen) {
      return next(new ErrorHandler("Canteen not found", 404));
  }

  assignCanteen ? assignCanteen.canteen_admin_id = canteen_admin_id : assignCanteen.canteen_admin_id

  await assignCanteen.save()

  res.json({
      success: true,
      message: 'canteen admin is updated successfully'
  })
})

/// Display canteen admin all canteens

exports.displayAdminCanteens = asyncCatchHandler(async (req, res, next) => {

  const { canteen_admin_id } = req.params;

  const canteen_admin = await userModel.findOne({ _id: mongoose.Types.ObjectId(canteen_admin_id), user_role: 'canteen_admin', user_status: 'Active' })
 
  if (!canteen_admin) {
      return next(new ErrorHandler("Canteen admin not found", 404));
  }
 
  const canteenAdmin = await canteenModel.find({canteen_admin_id:mongoose.Types.ObjectId(canteen_admin_id) ,canteen_status: 'Active' })

  if (canteenAdmin && canteenAdmin.length<1) {
      return next(new ErrorHandler("Canteen admin don't have any canteen", 404));
  }

 

  res.json({
      success: true,
      data:canteenAdmin
  })
})

///// Display All Machine under 1 canteen

exports.machineCanteen = asyncCatchHandler(async (req, res, next) => {
  const { canteen_id } = req.params;

  const canteen_Link_Machine = await machineModel.find({
    canteen_id: mongoose.Types.ObjectId(canteen_id),
    machine_status:'Active'
  });

  if (canteen_Link_Machine.length < 1) {
    return next(new ErrorHandler("Canteen has no  machine", 404));
  }

  res.json({
    success: true,
    All_Machines: canteen_Link_Machine,
  });
});

//// Assign or update Canteen to supplier

exports.assignCanteen = asyncCatchHandler(async (req, res, next) => {
  const { canteen_id, supplier_id } = req.params;

  const user = await userModel.findById({
    _id: mongoose.Types.ObjectId(supplier_id),user_status:'Active'
  });
  if (!user) {
    return next(new ErrorHandler("User not found", 404));
  }
  const assignCanteen = await canteenModel.findOne({
    _id: mongoose.Types.ObjectId(canteen_id),
    canteen_status:'Active'
  });

  if (!assignCanteen) {
    return next(new ErrorHandler("Canteen not found", 404));
  }

  user ? (assignCanteen.supplier_id = supplier_id) : assignCanteen.supplier_id;

  await assignCanteen.save();

  res.json({
    success: true,
    message: "canteen is assigned successfully",
  });
});

///// Display All Canteen under 1 Supplier
exports.supplierCanteen = asyncCatchHandler(async (req, res, next) => {
  const { supplier_id } = req.params;

  const user = await userModel.findById({
    _id: mongoose.Types.ObjectId(supplier_id),
    user_status:'Active'
  });

  if (!user) {
    return next(new ErrorHandler("User not found", 404));
  }
  const assignCanteen = await canteenModel
    .find({ supplier_id: mongoose.Types.ObjectId(supplier_id),canteen_status:'Active' })
    .populate({
      path: "supplier_id",
      select: {user_password:0,user_permission:0 },
    });
  if (assignCanteen.length < 1) {
    return next(new ErrorHandler("Canteen not found", 404));
  }

  res.json({
    success: true,
    assignCanteen,
  });
});
