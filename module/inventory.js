const inventoryModel = require('../model/inventory')

exports.inventoryFunction = async (cart, res, next) => {
    try {
        const updateModel = await inventoryModel.findOneAndUpdate({ productId: cart[0].product_id });

        if (!updateModel) {

            return response = {
                success: false,
                message: 'Inventory not found'
            }
        }
        if (cart[0].quantity && updateModel.quantity < cart[0].quantity) {

            return respone = {
                success: false
            }
        }

        cart[0].quantity ? updateModel.quantity = updateModel.quantity - Number(cart[0].quantity) : updateModel.quantity;
        cart[0].quantity ? updateModel.reservation = updateModel.reservation + Number(cart[0].quantity) : updateModel.reservation

        await updateModel.save();

        next()
    }
    catch (err) {
        res.json({ message: err.message })
    }
}

