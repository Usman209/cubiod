const sgMail = require('@sendgrid/mail');

const sendGridApi = process.env.USER_API;
require('dotenv').config();
sgMail.setApiKey(sendGridApi);
const userAddMessage = (name, email) => {
    // console.log(process.env.SENDER_EMAIL, 'Email');

    sgMail.send({
        to: email,
        from: process.env.SENDER_EMAIL,
        subject: 'Welcome in Cuboid',
        html: `<strong>  Welcome ${name}, Your account has been created on Cuboid. Please visit Cuboid office for further Detail. </strong>`,
    });


};
const userChangePasswordMessage = (name, email) => {

    sgMail.send({
        to: email,
        from: process.env.SENDER_EMAIL,
        subject: 'Password Rest',
        html: `<strong>  Welcome ${name}, Your password has been changed from Cuboid.Please visit Cuboid office for further Detail, </strong>`,
    });


};

const promotionUpdate = (user_name, user_email,promo_name,promo_code,promo_value,promo_type,) => {
    // console.log(process.env.SENDER_EMAIL, 'Email');
console.log(user_name,user_email,promo_name,promo_code,promo_value,promo_type)
    sgMail.send({
        to: user_email,
        from: process.env.SENDER_EMAIL,
        subject: 'Promotion Offer in Cuboid',
        html: `
        <h3>Cuboid Promotion Offer:<strong> ${promo_name}</strong></h3>
        <h3>promo code ${promo_code}</h3>
        <p><strong>Helo ${user_name}, Cuboid have ${promo_name} offer. Please visit cuboid to avail ${promo_value} ${promo_type} discount.  </strong> </p>
        `,
    });


};
const discountUpdate = (user_name, user_email,discount_name,discount_value,discount_type) => {
    // console.log(process.env.SENDER_EMAIL, 'Email');
console.log(user_name,user_email,discount_name,discount_value,discount_type)
    sgMail.send({
        to: user_email,
        from: process.env.SENDER_EMAIL,
        subject: 'Discount Offers in Cuboid',
        html: `
        <h3>Cuboid Discount Offer:<strong> ${discount_name}</strong></h3>
      
        <p><strong>Helo ${user_name}, Cuboid have ${discount_name} offer. Please visit cuboid to avail ${discount_value} ${discount_type} discount.  </strong> </p>
        `,
    });


};


module.exports = {
    userAddMessage,
    userChangePasswordMessage,
    promotionUpdate,
    discountUpdate
};