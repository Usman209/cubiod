const sgMail = require('@sendgrid/mail');

const sendGridApi = process.env.USER_API;
require('dotenv').config();
sgMail.setApiKey(sendGridApi);

const emailRegistration = (name, email) => {

    sgMail.send({
        to: email, // Change to your recipient
        from: process.env.SENDER_EMAIL, // Change to your verified sender
        subject: 'Welcome in Vendering app',
        text: `Welcome ${name} for creating your acount on our Website`,
        html: '<stron>Thanks for signup on cuboic </strong>',
    });


};
const forgotPassword = (email, data) => {
    const tok = `http://localhost:3000/reset-password?token=${data}`;
    sgMail.send({
        to: email,
        from: process.env.SENDER_EMAIL,
        subject: 'Forgot Password Email',
        html: `<strong>Please click on this link to forgot your cuboid password ${tok}</strong>`,
    });

   


};
const resetPassword = (email, data) => {
    // console.log(email, data);
    const tok = `http://localhost:3000/reset-password?token=${data}`;
    sgMail.send({
        to: email,
        from: process.env.SENDER_EMAIL,
        subject: 'Forgot Email',
        text: `Please click on that link to forgot passwor ${tok}`,
        html: `< strong > Please click on that link to forgot password ${tok} </strong > `,
    });


};
module.exports = {
    emailRegistration,
    forgotPassword
};